class Animal {
  constructor(nombre, genero) {
    this.nombre = nombre;
    this.genero = genero;
  };

  sonar() {
    console.log('Hago sonidos porque estoy vivo');
  };

  saludar() {
    console.log(`Hola me llamo ${this.nombre}`);
  };
};

class Perro extends Animal {
  constructor(nombre, genero, tamanio) {
    super(nombre, genero);
    this.tamanio = tamanio;
    this.raza = null;
  };

  sonar() {
    console.log('Soy un perro y mi sonido es un ladrido');
  };

  ladrar() {
    console.log('Guauuu guauuu');
  };

  // Un metodo estatico se puede ejecutar  sin necesidad de instanciar la clase
  static queEres() {
    console.log('Los perros somos animales mamiferos que pertenecemos a la familia de los caninos. Somos considerados los mejores amigos del hombre');
  };

  // Los getters y setters son metodos especiales que nos permiten establecer y obtener los valores de atributos de nuestra clase
  get getRaza() {
    return this.raza;
  };

  set setRaza(raza) {
    this.raza = raza;
  };
};

Perro.queEres();

const mimi = new Animal('Mimi', 'Hembra');
console.log(mimi);
mimi.saludar();
mimi.sonar();

const scooby = new Perro('Scooby', 'Macho', 'Gigante');
console.log(scooby);
scooby.saludar();
scooby.sonar();
scooby.ladrar();

console.log(scooby.getRaza);
scooby.setRaza = 'Gran Danes';
console.log(scooby.getRaza);