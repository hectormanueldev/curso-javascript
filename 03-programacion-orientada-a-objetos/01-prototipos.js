/*
  Las clases son un modelo a seguir

  Los objetos son una instancia de una clase

  - atributos: caracteristica o propiedad del objeto (variables dentro de un objeto)
  - metodos: son las acciones que un objeto puede realizar (son funciones dentro de un objeto)
*/

const animal = {
  nombre: 'Snoopy',
  sonar() {
    console.log('Hago sonidos porque estoy vivo');
  }
};

console.log(animal);

// Funcion constructora
function Animal(nombre, genero) {
  // Atributos
  this.nombre = nombre;
  this.genero = genero;

  // Metodos
  this.sonar = function () {
    console.log('Hago sonidos porque estoy vivo');
  };

  this.saludar = function () {
    console.log(`Hola me llamo ${this.nombre} y soy ${this.genero}`);
  };
};

const snoopy = new Animal('Snoopy', 'Macho');
console.log(snoopy);

const lolaBunny = new Animal('Lola Bunny', 'Hembra');
console.log(lolaBunny);

// Funcion constructora donde asignamos los metodos al prototipo, no a la funcion como tal
function Animales(nombre, genero) {
  this.nombre = nombre;
  this.genero = genero;
};

// Metodos agregados al prototipo de la funcion constructora
Animales.prototype.sonar = function () {
  console.log('Hago sonidos porque estoy vivo');
};

Animales.prototype.saludar = function () {
  console.log(`Hola me llamo ${this.nombre} y soy ${this.genero}`);
};

const guardian = new Animales('Guardian', 'Macho');
const pakita = new Animales('Pakita', 'Hembra');

console.log(guardian);
guardian.saludar();
console.log(pakita);
pakita.saludar();