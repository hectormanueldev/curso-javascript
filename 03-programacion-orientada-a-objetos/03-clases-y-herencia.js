// El constructor es un metodo especial que se ejecuta en el momento de instanciar la clase
class Animal {
  constructor(nombre, genero) {
    this.nombre = nombre;
    this.genero = genero;
  };

  // Metodos
  sonar() {
    console.log('Hago sonidos porque estoy vivo');
  };

  saludar() {
    console.log(`Hola me llamo ${this.nombre}`);
  };
};

class Perro extends Animal {
  constructor(nombre, genero, tamanio) {
    super(nombre, genero);
    this.tamanio = tamanio;
  };

  sonar() {
    console.log('Soy un perro y mi sonido es una ladrido');
  };

  ladrar() {
    console.log('Guauuu guauuu');
  };
};

const mimi = new Animal('Mimi', 'Hembra');
console.log(mimi);
mimi.saludar();
mimi.sonar();

const scooby = new Perro('Scooby', 'Macho', 'Gigante');
console.log(scooby);
scooby.saludar();
scooby.sonar();
scooby.ladrar();