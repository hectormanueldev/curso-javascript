// Funcion constructora donde asignamos los metodos al prototipo, no a la funcion como tal
function Animal(nombre, genero) {
  this.nombre = nombre;
  this.genero = genero;
};

// Metodos agregados al prototipo de la funcion constructora
Animal.prototype.sonar = function () {
  console.log('Hago sonidos porque estoy vivo');
};

Animal.prototype.saludar = function () {
  console.log(`Hola me llamo ${this.nombre} y soy ${this.genero}`);
};

// Herencia prototipica
function Perro(nombre, genero, tamanio) {
  this.super = Animal;
  this.super(nombre, genero);
  this.tamanio = tamanio;
};

// Perro hereda de Animal
Perro.prototype = new Animal();
Perro.prototype.constructor = Perro();

// Sobreescritura de metodos del prototipo padre en el hijo
Perro.prototype.sonar = function () {
  console.log('Soy un perro y mi sonido es un ladrido');
};

Perro.prototype.ladrar = function () {
  console.log('Guauuu guauuu');
};

const snoopy = new Perro('Snoopy', 'Macho', 'Mediano');
console.log(snoopy);
snoopy.saludar();
snoopy.sonar();
snoopy.ladrar();

const lolaBunny = new Animal('Lola Bunny', 'Hembra');
console.log(lolaBunny);
lolaBunny.saludar();
lolaBunny.sonar();