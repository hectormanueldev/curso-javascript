console.log(this);
console.log(window);
console.log(this === window);

// Crea una variable de ambito global
this.nombre = 'contexto global';
console.log(this.nombre);

function imprimir() {
  console.log(this.nombre);
}

imprimir();

const obj = {
  nombre: 'contexto objeto',
  imprimir: function () {
    console.log(this.nombre);
  }
};

obj.imprimir();

const obj2 = {
  nombre: 'contexto objeto 2',
  imprimir
};

obj2.imprimir();

// Crea conflicto el this en las arrow functions, porque no crea su scope
const obj3 = {
  nombre: 'contexto objeto 3',
  imprimir: () => {
    console.log(this.nombre);
  }
};

obj3.imprimir();

function Persona(nombre) {
  // const that = this;
  // that.nombre = nombre;
  // return () => console.log(that.nombre);

  this.nombre = nombre;
  // return console.log(this.nombre);

  // Toma el contexto global this
  // return function () {
  //   console.log(this.nombre);
  // }
  return () => console.log(this.nombre);
}

let hector = Persona('hector');

hector();