this.lugar = 'contexto global';

function saludar(saludo, aQuien) {
  console.log(`${saludo} ${aQuien} desde el ${this.lugar}`);
}

saludar('Hola', 'Mojoncio');

const obj = {
  lugar: 'contexto objeto'
};

saludar.call(obj, 'hola', 'hector');
saludar.call(null, 'hola', 'hector');
saludar.call(this, 'hola', 'hector');

saludar.apply(obj, ['adios', 'hector']);
saludar.apply(null, ['adios', 'hector']);
saludar.apply(this, ['adios', 'hector']);

// bind sirve para enlazar
this.nombre = 'window';

const persona = {
  nombre: 'hector',
  apellido: 'santos',
  edad: 20,
  saludar: function () {
    console.log(`Hola ${this.nombre}`);
  }
};

persona.saludar();

const otraPersona = {
  saludar: persona.saludar.bind(persona)
}

otraPersona.saludar();