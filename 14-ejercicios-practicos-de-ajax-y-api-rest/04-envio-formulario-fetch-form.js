const d = document;

function contactForm() {
  const $form = d.querySelector('.contact-form');
  const $inputs = d.querySelectorAll('.contact-form [required]');

  $inputs.forEach((input) => {
    const $span = d.createElement('span');
    $span.id = input.name;
    $span.textContent = input.title;
    $span.classList.add('contact-form-error', 'none');
    input.insertAdjacentElement('afterend', $span);
  });

  d.addEventListener('keyup', (e) => {
    if (e.target.matches('.contact-form [required]')) {
      let pattern = e.target.pattern || e.target.dataset.pattern;

      if (pattern && e.target.value !== '') {
        let regex = new RegExp(pattern);

        return !regex.exec(e.target.value)
          ? d.getElementById(e.target.name).classList.add('is-active')
          : d.getElementById(e.target.name).classList.remove('is-active');
      }

      if (!pattern) {
        return e.target.value === ''
          ? d.getElementById(e.target.name).classList.add('is-active')
          : d.getElementById(e.target.name).classList.remove('is-active');
      }
    }
  });

  d.addEventListener('submit', async (e) => {
    e.preventDefault();
    alert('Enviando Formulario...');

    const $loader = d.querySelector('.contact-form-loader');
    const $response = d.querySelector('.contact-form-response');

    $loader.classList.remove('none');

    try {
      let options = {
        method: 'POST',
        body: new FormData(e.target),
      };

      const res = await fetch('https://formsubmit.co/ajax/hectormanueldev@gmail.com', options);
      const json = await res.json();

      console.log(json);

      $loader.classList.add('none');
      $response.classList.remove('none');
      $response.innerHTML = `<p>${json.message}</p>`;
      $form.reset();

      if (!res.ok) throw { status: res.status, statusText: res.statusText };
    } catch (err) {
      let message = err.statusText || 'Ocurrio un error al enviar, intenta nuevamente';
      $response.innerHTML = `<p>Error ${err.status}: ${message}</p>`;
      console.log(err);
    } finally {
      setTimeout(() => {
        $response.classList.add('none');
        $response.innerHTML = '';
      }, 3000);
    }
  });
}

d.addEventListener('DOMContentLoaded', contactForm);