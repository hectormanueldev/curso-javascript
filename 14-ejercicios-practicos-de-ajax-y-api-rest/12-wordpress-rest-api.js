const d = document;
const w = window;
const $site = d.getElementById('site');
const $posts = d.getElementById('posts');
const $loader = d.querySelector('.loader');
const $template = d.getElementById('post-template').content;
const $fragment = d.createDocumentFragment();

const DOMAIN = 'https://css-tricks.com';
const SITE = `${DOMAIN}/wp-json`;
const API_WP = `${SITE}/wp/v2`;
const POSTS = `${API_WP}/posts?_embed`;
const PAGES = `${API_WP}/pages`;
const CATEGORIES = `${API_WP}/categories`;

let page = 1;
let perPage = 5;

const getSiteData = async () => {
  try {
    let res = await fetch(SITE);
    let json = await res.json();

    if (!res.ok) throw { status: res.status, statusText: res.statusText };

    console.log(json);

    $site.innerHTML = `
      <h1>
        <a href="${json.url}" target="_blank">${json.name}</a>
      </h1>
      <p>${json.description}</p>
    `;
  } catch (err) {
    let message = err.statusText || 'Ocurrio un error al conectar al API';
    $site.innerHTML = `<p>Error ${err.status}: ${message}</p>`;
    console.log(err);
  }
};

const getPosts = async () => {
  $loader.style.display = 'block';
  try {
    let res = await fetch(`${POSTS}&page=${page}&per_page=${perPage}`);
    let json = await res.json();

    if (!res.ok) throw { status: res.status, statusText: res.statusText };

    console.log(json);

    json.forEach((el) => {
      let categories = '';
      let tags = '';

      el._embedded['wp:term'][0].forEach((el) => (categories += `<li>${el.name}</li>`));
      el._embedded['wp:term'][1].forEach((el) => (tags += `<li>${el.name}</li>`));

      $template.querySelector('.post-img').src = el.jetpack_featured_media_url;
      // $template.querySelector('.post-img').src = el._embedded['wp:featuredmedia'] ? el._embedded['wp:featuredmedia'][0].source_url : '';
      $template.querySelector('.post-title').innerHTML = el.title.rendered;
      $template.querySelector('.post-author').innerHTML = `
        <img src="${el._embedded.author[0].avatar_urls['24']}" alt="${el._embedded.author[0].name}">
        <figcaption>${el._embedded.author[0].name}</figcaption>
      `;
      $template.querySelector('.post-date').innerHTML = `Publicado el: ${new Date(el.date).toLocaleDateString()}`;
      $template.querySelector('.post-link').href = el.link;
      $template.querySelector('.post-excerpt').innerHTML = el.excerpt.rendered.replace(' [&hellip;]', ' ...');
      $template.querySelector('.post-categories').innerHTML = `
        <p>Categorias:</p>
        <ul>${categories}</ul>
      `;
      $template.querySelector('.post-tags').innerHTML = `
        <p>Etiquetas:</p>
        <ul>${tags}</ul>
      `;

      $template.querySelector('.post-content > article').innerHTML = el.content.rendered;

      let $clone = d.importNode($template, true);
      $fragment.appendChild($clone);
    });

    $posts.appendChild($fragment);
    $loader.style.display = 'none';
  } catch (err) {
    let message = err.statusText || 'Ocurrio un error al conectar al API';
    $posts.innerHTML = `<p>Error ${err.status}: ${message}</p>`;
    $loader.style.display = 'none';
    console.log(err);
  }
};

d.addEventListener('DOMContentLoaded', (e) => {
  getSiteData();
  getPosts();
});

// Scroll infinito
w.addEventListener('scroll', (e) => {
  const { scrollTop, clientHeight, scrollHeight } = d.documentElement;

  if (scrollTop + clientHeight >= scrollHeight) {
    page++;
    getPosts();
  }
});