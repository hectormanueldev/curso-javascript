# Blog con Markdown y Showdown.js

## Isomorfismo

Hoy _Javascript_, es el unico lenguaje capaz de ejecutarse en las 3 capas de una aplicacion:

1._Frontend_ (con _JavaScript_).
1._Backend_ (con _Node.js_).
1._Persistencia de Datos_ (con _MongoDB_, _Couch DB_, _Firebase_, etc).

Con JavaScript puedes:

- Diseño y Desarrollo Web.
- Hacer Videojuegos.
- Experiencias _3D_, Realidad Aumentada, Realidad Virtual.
- Controlar _Hardware_ (drones, _robots_, electrodomesticos, _wearables_, etc).
- Aplicaciones Hibridas y Moviles.
- Aprendizaje Automatico.
- etc.