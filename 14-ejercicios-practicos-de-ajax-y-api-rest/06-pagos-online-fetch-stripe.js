const d = document;
const $productos = d.getElementById('productos');
const $template = d.getElementById('productos-template').content;
const $fragment = d.createDocumentFragment();

const keysStripe = {
  keyPublic: 'Your Key Public',
  keySecret: 'Your Key Secret',
};

const options = {
  headers: {
    Authorization: `Bearer ${keysStripe.keySecret}`,
  },
};

let products;
let prices;

const moneyFormat = (num) => `$${num.slice(0, -2)}. ${num.slice(-2)}`;

Promise.all([fetch('https://api.stripe.com/v1/products', options), fetch('https://api.stripe.com/v1/prices', options)])
  .then((responses) => Promise.all(responses.map((res) => res.json())))
  .then((json) => {
    products = json[0].data;
    prices = json[1].data;

    prices.forEach((el) => {
      let productData = products.filter((product) => product.id === el.product);

      $template.querySelector('.producto').setAttribute('data-price', el.id);
      $template.querySelector('img').src = productData[0].images[0];
      $template.querySelector('img').alt = productData[0].name;
      $template.querySelector('figcaption').innerHTML = `
        ${productData[0].name}
        <br>
        ${moneyFormat(el.unit_amount_decimal)} ${el.currency.toUpperCase()}
      `;

      let $clone = d.importNode($template, true);
      $fragment.appendChild($clone);
    });

    $productos.appendChild($fragment);
  })
  .catch((err) => {
    console.log(err);
    let message = err.statusText || 'Ocurrio un error al conectarse al API de Stripe';
    $tacos.innerHTML = `<p>Error ${err.status}: ${message}</p>`;
  });

d.addEventListener('click', (e) => {
  if (e.target.matches('.producto *')) {
    let priceId = e.target.parentElement.getAttribute('data-price');

    Stripe(keysStripe.keyPublic).redirectToCheckout({
      lineItems: [
        {
          price: priceId,
          quantity: 1,
        },
      ],
      // Para cuando las compras son por suscripcion
      // mode: 'subscription',
      // Para cuando las compras son en una sola exhibicion
      mode: 'payment',
      // Urls de redireccion
      successUrl: 'http://127.0.0.1:5500/assets/html/stripe-success.html',
      cancelUrl: 'http://127.0.0.1:5500/assets/html/stripe-cancel.html',
    })
    .then((result) => {
      if (result.error) {
        $productos.insertAdjacentHTML('afterend', result.error.message);
      }
    });
  }
});