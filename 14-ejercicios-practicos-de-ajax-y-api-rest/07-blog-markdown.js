const d = document;
const $main = d.querySelector('main');

const converterMarkdown = async () => {
  try {
    let res = await fetch('assets/blog.md');
    let text = await res.text();

    $main.innerHTML = new showdown.Converter().makeHtml(text);

    if (!res.ok) throw { status: res.status, statusText: res.statusText };
  } catch (err) {
    let message = err.statusText || 'Ocurrio un error';
    $main.innerHTML = `<p>Error ${err.status}: ${message}</p>`;
  }
};

d.addEventListener('DOMContentLoaded', converterMarkdown);