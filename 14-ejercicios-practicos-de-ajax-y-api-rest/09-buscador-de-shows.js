const d = document;
const $shows = d.getElementById('shows');
const $template = d.getElementById('show-template').content;
const $fragment = d.createDocumentFragment();

d.addEventListener('keypress', async (e) => {
  if (e.target.matches('#search')) {
    if (e.key === 'Enter') {
      try {
        $shows.innerHTML = `<img class="loader" src="assets/img/loader.svg" alt="Cargando...">`;

        let query = e.target.value.toLowerCase();
        let api = `http://api.tvmaze.com/search/shows?q=${query}`;

        const res = await fetch(api);
        const json = await res.json();

        if (!res.ok) throw { status: res.status, statusText: res.statusText };

        if (json.length === 0) {
          e.target.value = '';
          $shows.innerHTML = `<p class="message">No existen resultados para <span>${query}</span></p>`;
        } else {
          json.forEach((el) => {
            $template.querySelector('h3').textContent = el.show.name;
            $template.querySelector('div').innerHTML = el.show.summary ? el.show.summary : 'Without description';
            $template.querySelector('img').src = el.show.image
              ? el.show.image.medium
              : 'http://static.tvmaze.com/images/no-img/no-img-portrait-text.png';
            $template.querySelector('img').alt = el.show.name;
            $template.querySelector('a').href = el.show.url ? el.show.url : '#';
            $template.querySelector('a').target = el.show.url ? '_blank' : '_self';
            $template.querySelector('a').textContent = el.show.url ? 'Ver Más' : '';

            let $clone = d.importNode($template, true);
            $fragment.appendChild($clone);

            e.target.value = '';
            $shows.innerHTML = '';
          });
        }

        $shows.appendChild($fragment);
      } catch (err) {
        let message = err.statusText || 'Ocurrio un error al conectar al API';
        $shows.innerHTML = `<p class="message">Error ${err.status}: ${message}</p>`;
      }
    }
  }
});