const d = document;
const $main = d.querySelector('main');
const $links = d.querySelector('.links');

let urlPokeAPI = 'https://pokeapi.co/api/v2/pokemon/';

async function loadPokemons(url) {
  try {
    $main.innerHTML = `<img class="loader" src="assets/img/loader.svg" alt="Cargando...">`;

    let res = await fetch(url);
    let json = await res.json();

    let $template = '';
    let $prevLink;
    let $nextLink;

    const letraCapital = (letra) => `${letra.charAt(0).toUpperCase()}${letra.slice(1)}`;

    if (!res.ok) throw { status: res.status, statusText: res.statusText };

    for (let i = 0; i < json.results.length; i++) {
      try {
        let res = await fetch(json.results[i].url);
        let pokemon = await res.json();
        console.log(pokemon);
        if (!res.ok) throw { status: res.status, statusText: res.statusText };

        $template += `
          <figure class="card">
            <img class="card-img" src="${pokemon.sprites.front_default}" alt="${letraCapital(pokemon.name)}">
            <figcaption class="card-title">${letraCapital(pokemon.name)}</figcaption>
          </figure>
        `;
      } catch (err) {
        let message = err.statusText || 'Ocurrio un error';
        $template += `
          <figure>
            <figcaption>Error ${err.status}: ${message}</figcaption>
          </figure>
        `;
      }
    }

    $main.innerHTML = $template;
    $prevLink = json.previous ? `<a href="${json.previous}">⏮️</a>` : '';
    $nextLink = json.next ? `<a href="${json.next}">⏭️</a>` : '';
    $links.innerHTML = `${$prevLink} ${$nextLink}`;
  } catch (err) {
    let message = err.statusText || 'Ocurrio un error';
    $main.innerHTML = `<p>Error ${err.status}: ${message}</p>`;
  }
}

d.addEventListener('DOMContentLoaded', (e) => loadPokemons(urlPokeAPI));

d.addEventListener('click', (e) => {
  if (e.target.matches('.links a')) {
    e.preventDefault();
    loadPokemons(e.target.getAttribute('href'));
  }
});