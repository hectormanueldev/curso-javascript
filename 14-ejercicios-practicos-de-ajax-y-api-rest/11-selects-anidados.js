const d = document;
const $selectPrimary = d.getElementById('select-primary');
const $selectSecondary = d.getElementById('select-secondary');
const $selectTertiary = d.getElementById('select-tertiary');
const $errorEstados = d.querySelector('.error-estados');
const $errorMunicipios = d.querySelector('.error-municipios');
const $errorColonias = d.querySelector('.error-colonias');

const loadState = async () => {
  try {
    let res = await fetch('https://api-sepomex.hckdrk.mx/query/get_estados', { mode: 'no-cors' });
    let json = await res.json();

    if (!res.ok) throw { status: res.status, statusText: res.statusText };

    let $options = `<option value="">Elige un Estado</option>`;

    json.response.estado.forEach((el) => {
      $options += `<option value="${el}">${el}</option>`;
    });

    $selectPrimary.innerHTML = $options;
  } catch (err) {
    let message = err.statusText || 'Ocurrio un error al conectar al API';
    $errorEstados.innerHTML = `Error ${err.status}: ${message}`;
  }
};

const loadMunicipality = async (state) => {
  try {
    let res = await fetch(`https://api-sepomex.hckdrk.mx/query/get_municipio_por_estado/${state}`);
    let json = await res.json();

    if (!res.ok) throw { status: res.status, statusText: res.statusText };

    let $options = `<option value="">Elige un Municipio</option>`;

    json.response.municipios.forEach((el) => {
      $options += `<option value="${el}">${el}</option>`;
    });

    $selectSecondary.innerHTML = $options;
  } catch (err) {
    console.log(err);
    let message = err.statusText || 'Ocurrio un error al conectar al API';
    $errorMunicipios.innerHTML = `Error ${err.status}: ${message}`;
  }
};

const loadColonies = async (municipality) => {
  try {
    let res = await fetch(`https://api-sepomex.hckdrk.mx/query/get_colonia_por_municipio/${municipality}`);
    let json = await res.json();

    if (!res.ok) throw { status: res.status, statusText: res.statusText };

    let $options = `<option value="">Elige una Colonia</option>`;

    json.response.colonia.forEach((el) => {
      $options += `<option value="${el}">${el}</option>`;
    });

    $selectTertiary.innerHTML = $options;
  } catch (err) {
    console.log(err);
    let message = err.statusText || 'Ocurrio un error al conectar al API';
    $errorColonias.innerHTML = `Error ${err.status}: ${message}`;
  }
};

d.addEventListener('DOMContentLoaded', loadState);
$selectPrimary.addEventListener('change', (e) => loadMunicipality(e.target.value));
$selectSecondary.addEventListener('change', (e) => loadColonies(e.target.value));