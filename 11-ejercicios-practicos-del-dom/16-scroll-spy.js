export default function scrollSpy() {
  const $sections = document.querySelectorAll('section[data-scroll-spy]');

  const intersectionCallback = (entries) => {
    entries.forEach((entry) => {
      const id = entry.target.getAttribute('id');

      if (entry.isIntersecting) {
        document.querySelector(`a[data-scroll-spy][href="#${id}"]`).classList.add('active');
      } else {
        document.querySelector(`a[data-scroll-spy][href="#${id}"]`).classList.remove('active');
      }
    });
  };

  const options = {
    // root: document,
    // rootMargin: '-450px',
    threshold: 0.5
  };

  const observer = new IntersectionObserver(intersectionCallback, options);

  $sections.forEach((el) => {
    observer.observe(el);
  });
};