export default function smartVideo() {
  const $video = document.querySelectorAll('video[data-smart-video]');

  const intersectionCallback = (entries) => {
    entries.forEach((entry) => {
      if (entry.isIntersecting) {
        entry.target.play();
      } else {
        entry.target.pause();
      }

      // Pausar el video al cambiar de pestaña
      window.addEventListener('visibilitychange', (e) =>
        document.visibilityState === 'visible'
        ? entry.target.play()
        : entry.target.pause()
      );
    });
  };

  const options = {
    threshold: 0.5
  };

  const observer = new IntersectionObserver(intersectionCallback, options);

  $video.forEach((el) => {
    observer.observe(el);
  });
};