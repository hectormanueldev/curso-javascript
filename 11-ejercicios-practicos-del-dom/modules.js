import hamburgerMenu from './01-menu-hamburguesa.js';
import { digitalClock, alarm } from './02-reloj-digital.js';
import { moveBall, shortcuts } from './03-teclado.js';
import countdown from './04-cuenta-regresiva.js';
import scrollTopBtn from './05-scroll-top-btn.js';
import darkTheme from './06-tema-dark-light.js';
import responsiveMedia from './07-responsive-con-js.js';
import responsiveTester from './08-responsive-tester.js';
import userDevicesInfo from './09-deteccion-de-dispositivos.js';
import networkStatus from './10-deteccion-estado-red.js';
import webCam from './11-deteccion-webcam.js';
import getGeolocation from './12-geolocalizacion.js';
import searchFilters from './13-filtro-de-busqueda.js';
import draw from './14-sorteo-digital.js';
import slider from './15-slider-responsive.js';
import scrollSpy from './16-scroll-spy.js';
import smartVideo from './17-video-inteligente.js';
import contactFormValidation from './18-validacion-formulario.js';
import speechReader from './19-narrador.js';

document.addEventListener('DOMContentLoaded', (e) => {
  hamburgerMenu('.panel-btn', '.panel', '.menu a');
  digitalClock('#reloj', '#activar-reloj', '#desactivar-reloj');
  alarm('./assets/audio/alarma.mp3', '#activar-alarma', '#desactivar-alarma');
  countdown('countdown', 'November 13, 2022 15:00:00', 'Hoy es tu Cumpleaños 🎁🎁🎁');
  scrollTopBtn('.scroll-top-btn');
  responsiveMedia(
    'youtube',
    '(min-width: 1024px)',
    `<a href="https://www.youtube.com/watch?v=6IwUl-4pAzc&list=PLvq-jIkSeTUZ6QgYYO3MwG9EMqC-KoLXA&index=92" target="_blank" rel="noopener">Ver Vídeo</a>`,
    `<iframe width="560" height="315" src="https://www.youtube.com/embed/6IwUl-4pAzc" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
  );
  responsiveMedia(
    'gmaps',
    '(min-width: 1024px)',
    `<a href="https://goo.gl/maps/jHkDrDVfh7ArYXE78" target="_blank" rel="noopener">Ver Mapa</a>`,
    `<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d963935.8028381707!2d-100.16542713274555!3d19.30956072626616!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d201f60a8f816b%3A0xca7e9b8e9149aac2!2sGoogle!5e0!3m2!1ses-419!2smx!4v1615263808063!5m2!1ses-419!2smx" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>`
  );
  responsiveTester('responsive-tester');
  userDevicesInfo('user-devices');
  webCam('webcam');
  getGeolocation('geolocation');
  searchFilters('.card-filter', '.card');
  draw('#winner-btn', '.player');
  slider();
  scrollSpy();
  smartVideo();
  contactFormValidation();
});

document.addEventListener('keydown', (e) => {
  moveBall(e, '.ball', '.stage');
  shortcuts(e);
});

darkTheme('.dark-theme-btn', 'dark-mode');
networkStatus();
speechReader();