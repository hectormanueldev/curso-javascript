export default function getGeolocation(id) {
  const $div = document.getElementById(id);

  const options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
  };

  const sucess = (position) => {
    let coords = position.coords;
    $div.innerHTML = `
      <p>Tu posicion actual es:</p>
      <ul>
        <li>Latitude: <b>${coords.latitude}</b></li>
        <li>Longitud: <b>${coords.longitude}</b></li>
        <li>Precision: <b>${coords.accuracy}</b> metros</li>
      </ul>
      <a href="https://www.google.com/maps/@${coords.latitude},${coords.longitude},20z" target="_blank">Ver en Google Maps</a>
    `;
  }

  const error = (err) => {
    $div.innerHTML = `<p><mark>Error ${err.code} : ${err.message}</mark></p>`;
  }

  navigator.geolocation.getCurrentPosition(sucess, error, options);
};