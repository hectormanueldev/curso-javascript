export const PI = Math.PI;
export let usuario = 'Hector';

// Para exportar de manera por defecto una variable o una constante se hace de la siguiente manera
let password = 'qwerty123456';
// export default password;

// export default solo se puede usar una vez por archivo js
export default function saludar() {
  console.log('Hola modulos +ES6');
};

export class Saludar {
  constructor() {
    console.log('Hola clases +ES6');
  };
};