/*
  Las expresiones regulares son una secuencia de caracteres que forma un patron de busqueda principalmente utilizada para la busqueda de patrones de cadena de caracteres
*/

// Primera forma de declarar una expresion regular
let cadena = 'Son una secuencia de caracteres que forma un patron de busqueda principalmente utilizada para la busqueda de patrones de cadena de caracteres';

let expReg = new RegExp('busqueda', 'ig');

// Segunda forma de declarar una expresion regular
let regExp = /de/ig;
let reg = /de{1,}/ig;

// El metodo "test()" ejecuta la busqueda de una ocurrencia entre una expresion regular y una cadena especificada, devuelve true o false
console.log(expReg.test(cadena));
console.log(regExp.test(cadena));
console.log(reg.test(cadena));

// El metodo "exec()" ejecuta una busqueda sobre las coincidencias de una expresion regular en una cadena especifica, devuelve el resultado como array o null
console.log(expReg.exec(cadena));
console.log(regExp.exec(cadena));
console.log(reg.exec(cadena));

// El metodo "match()" se usa para obtener todas las concurrencias de una expresion regular dentro de una cadena
console.log(cadena.match(expReg));
console.log(cadena.match(regExp));
console.log(cadena.match(reg));