alert('Hola esto es una alerta');
confirm('Hola esto es una confirmacion');
prompt('Hola esto es un prompt y le permite al usuario ingresar un valor');

let alerta = alert('Hola esto es una alerta');
console.log(alerta);

let confirmacion = confirm('Hola esto es una confirmacion');
console.log(confirmacion);

let aviso = prompt('Hola esto es un prompt y le permite al usuario ingresar un valor');
console.log(aviso);