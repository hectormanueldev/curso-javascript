(function () {
  console.log('Mi primer IIFE');
})();

(function (d, w, c) {
  console.log('Mi segunda IIFE');
  console.log(d);
  console.log(w);
  console.log(c);
})(document, window, console);

let result = (function () {
  let name = 'hector';
  return name;
})();

console.log(result);

// Clasica
(function () {
  console.log('Version clasica');
})();

// Crockford (JavaScript The Good Parts)
((function () {
  console.log('Version crockford');
})());

// Unaria
+function () {
  console.log('Version unaria');
}();

// Facebook
!function () {
  console.log('Version facebook');
}();