// La propiedad "PI" representa la relacion entre la longitud de la circunferencia de un circulo y su diametro
console.log(Math.PI);

// El metodo "abs()" retorna el valor absoluto de un numero
console.log(Math.abs(7.8));
console.log(Math.abs(-7.8));

// El metodo "ceil()" devuelve el entero mayor o igual mas proximo a un numero dado
console.log(Math.ceil(7.8));

// El metodo "floor()" devuelve el maximo entero menor o igual a un numero
console.log(Math.floor(7.8));

// El metodo "round()" retorna el valor de un numero redondeado al entero mas cercano
console.log(Math.round(7.8));
console.log(Math.round(7.2));

// El metodo "sqrt()" retorna la raiz cuadrada de un numero
console.log(Math.sqrt(2));
console.log(Math.sqrt(81));

// El metodo "pow()" devuelve la base elevada al exponente
console.log(Math.pow(2, 5));

// El metodo "sign()" retorna el signo de un numero, indicando si el numero es positivo, negativo o cero
console.log(Math.sign(-7.8));
console.log(Math.sign(7.8));
console.log(Math.sign('hola'));

// El metodo "random()" retorna un punto flotante, un numero pseudoaleatorio dentro del rango
console.log(Math.random());
console.log(Math.round(Math.random() * 1000));