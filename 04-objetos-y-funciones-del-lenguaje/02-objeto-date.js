let fechaActual = new Date();
console.log(fechaActual);

// El metodo "getDate()" devuelve el dia del mes
console.log(fechaActual.getDate());

// El metodo "getDay()" devuelve el dia de la semana
console.log(fechaActual.getDay());

// El metodo "getMonth" devuelve el mes
console.log(fechaActual.getMonth());

// El metodo "getFullYear()" devuelve el año de la fecha
console.log(fechaActual.getFullYear());

// El metodo "getHours()" devuelve la hora de la fecha
console.log(fechaActual.getHours());

// El metodo "getMinutes()" devuelve los minutos de la fecha
console.log(fechaActual.getMinutes());

// El metodo "getSeconds()" devuelve los segundos de la fecha
console.log(fechaActual.getSeconds());

// El metodo "getMilliseconds()" devuelve los milisegundos de la fecha
console.log(fechaActual.getMilliseconds());

// El metodo "toString()" devuelve una cadena que representa el Date
console.log(fechaActual.toString());

// El metodo "toDateString()" devuelve la porcion de la fecha
console.log(fechaActual.toDateString());

// El metodo "toLocaleString()" devuelve una cadena con la fecha
console.log(fechaActual.toLocaleString());

// El metodo "toLocaleDateString()" devuelve la fecha
console.log(fechaActual.toLocaleDateString());

// El metodo "toLocaleTimeString()" devuelve la hora
console.log(fechaActual.toLocaleTimeString());

// El metodo "getTimezoneOffset()" devuelve la diferencia de zona horaria en minutos
console.log(fechaActual.getTimezoneOffset());

// El metodo "getUTCDate()" devuelve el dia del mes segun la hora universal
console.log(fechaActual.getUTCDate());

// El metodo "getUTCHours()" devuelve la hora segun la hora universal
console.log(fechaActual.getUTCHours());

// El metodo "now()" devuelve el numero de milisegundos transcurridos desde las 00:00:00 UTC del 1 de enero de 1970
console.log(Date.now());

let cumpleHector = new Date(2000, 10, 13);
console.log(cumpleHector);