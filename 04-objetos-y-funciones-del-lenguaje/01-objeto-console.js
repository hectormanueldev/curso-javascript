console.error('Esto es un error');
console.warn('Esto es un aviso');
console.info('Esto es un mensaje informativo');
console.log('Un registro de lo que ha pasado en nuestra aplicacion');

let nombre = 'hector';
let apellido = 'santos';
let edad = 20;

console.log(nombre, apellido, edad);
console.log(`Hola mi nombre es ${nombre} ${apellido} y tengo ${edad} años`);
console.log('Hola mi nombre es %s %s y tengo %d años', nombre, apellido, edad);

console.log(window);
console.log(document);

// El metodo "dir()" despliega una lista interactiva de las propiedades del objeto JavaScript
console.dir(window);
console.dir(document);

// El metodo "group()" crea un nuevo grupo en linea en el registro de la consola web
console.group('Skills de hector');
console.log('Html');
console.log('Css');
console.log('JavaScript');

// El metodo "groupEnd()" sale del grupo en linea en la consola web
console.groupEnd();

// El metodo "groupCollapsed()" crea un nuevo grupo en linea en la consola web, este grupo se crea contraido
console.groupCollapsed('skills de hector');
console.log('html');
console.log('css');
console.log('javascript');
console.groupEnd();

// El metodo "clear()" borra la consola si el entorno lo permite
console.clear();

// El metodo "table()" muestra datos tabulares como una tabla
console.table(Object.entries(console).sort());

const numeros = [1, 2, 3, 4, 5];
console.table(numeros);

const vocales = ['a', 'e', 'i', 'o', 'u'];
console.table(vocales);

const people = [['jhon', 'smith'], ['jane', 'doe'], ['emily', 'jones']];
console.table(people);

const perro = {
  nombre: 'pakita',
  raza: 'corriente',
  color: 'cafe'
};

console.table(perro);

// El metodo "time()" inicia un temporizador que se puede utilizar para realizar un seguimiento de la duracion de una operacion
console.time('cuanto tiempo tarda mi codigo');

const arreglo = Array(100000);

for (let i = 0; i < arreglo.length; i++) {
  arreglo[i] = i;
}

// El metodo "timeEnd()" detiene un temporizador que se inicio previamente
console.timeEnd('cuanto tiempo tarda mi codigo');
console.log(arreglo);

// El metodo "count()" registra el numero de veces que count se ha llamado a esta llamada particular
for (let i = 0; i <= 100; i++) {
  console.count('codigo for');
  console.log(i);
}

// El metodo "assert()" escribe un mensaje de error en la consola si la asercion es falsa, si la afirmacion es cierta, no pasa nada
let x = 1;
let y = 2;
let pruebaXY = 'se espera que x siempre sea menor que y';

console.assert(x < y, [x, y, pruebaXY]);

let x2 = 10;
let y2 = 2;
let pruebaX2Y2 = 'se espera que x2 siempre sea menor que y2';

console.assert(x2 < y2, [x2, y2, pruebaX2Y2]);