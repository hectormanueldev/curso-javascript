import { aritmetica, aritmetica as calculos } from './09-modulo-aritmetica.js';
import saludar, { PI, usuario, Saludar } from './10-modulo-constantes.js';

console.log(aritmetica.restar(3, 4));
console.log(calculos.sumar(3, 4));

saludar();
console.log(PI);
console.log(usuario);

let saludo = new Saludar();
saludo;