function sumar(a, b) {
  return a + b;
}

function restar(a, b) {
  return a - b;
}

// Creamos un objeto que englobe las dos funciones y asi se exporta el objeto
export const aritmetica = { sumar, restar };