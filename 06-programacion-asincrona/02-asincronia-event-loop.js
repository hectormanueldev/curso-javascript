/*
  Asincronia y el Event Loop

  Antes de explicar como funciona el modelo de JavaScript es importante entender algunos conceptos:

  - Procesamiento single thread y multi thread
  - Operaciones de cpu y operaciones de i/o
  - Operaciones concurrentes y paralelas
  - Operaciones bloqueantes y no bloqueantes
  - Operaciones sincronas y asincronas

  JavaScript un modelo asincrono y no bloqueante, con el loop de eventos implementado en un solo hilo (single thread) para operaciones de entrada y salida
*/

// Codigo sincrono bloqueante
(() => {
  console.log('Codigo sincrono');
  console.log('Inicio');

  function dos() {
    console.log('Dos');
  };

  function uno() {
    console.log('Uno');

    dos();

    console.log('Tres');
  };

  uno();

  console.log('Fin');
})();

// Codigo asincrono no bloqueante
(() => {
  console.log('Codigo asincrono');
  console.log('Inicio');

  function dos() {
    setTimeout(() => {
      console.log('Dos');
    }, 1000);
  };

  function uno() {
    setTimeout(() => {
      console.log('Uno');
    }, 0);

    dos();

    console.log('Tres');
  };

  uno();
  console.log('Fin');
})();