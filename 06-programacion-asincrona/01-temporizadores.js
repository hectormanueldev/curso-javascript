// setTimeout
setTimeout(() => {
  console.log('Ejecutando un setTimeout, esto se ejecuta una sola vez');
}, 1000);

// setInterval
setInterval(() => {
  console.log('Ejecutando un setInterval, esto se ejecuta indefinidamente cada cierto intervalo de tiempo');
}, 1000);

// Temporizador con setInterval para mostrar la hora local continuamente
setInterval(() => {
  console.log(new Date().toLocaleTimeString());
}, 1000);

// Cancelar un setTimeout
let temporizador1 = setTimeout(() => {
  console.log(new Date().toLocaleTimeString());
}, 1000);

clearTimeout(temporizador1);
console.log('Despues del clearTimeout');

// Cancelar un setInterval
let temporizador2 = setInterval(() => {
  console.log(new Date().toLocaleTimeString());
}, 1000);

clearInterval(temporizador2);
console.log('Despues del clearInterval');