function cuadradoCallback(value, callback) {
  setTimeout(() => {
    callback(value, Math.pow(value, 2));
  }, 0 | Math.random() * 1000);
};

cuadradoCallback(0, (value, result) => {
  console.log('Inicio callback');
  console.log(`Callback: ${value}, ${result}`);

  cuadradoCallback(1, (value, result) => {
    console.log(`Callback: ${value}, ${result}`);

    cuadradoCallback(2, (value, result) => {
      console.log(`Callback: ${value}, ${result}`);

      cuadradoCallback(3, (value, result) => {
        console.log(`Callback: ${value}, ${result}`);

        cuadradoCallback(4, (value, result) => {
          console.log(`Callback: ${value}, ${result}`);

          cuadradoCallback(5, (value, result) => {
            console.log(`Callback: ${value}, ${result}`);
            console.log('Fin callback');
            console.log('Callback hell');
            console.log('http://callbackhell.com/');
          });
        });
      });
    });
  });
});