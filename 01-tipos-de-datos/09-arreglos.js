// Declaracion de un array con let
const arrayA = [];
console.log(arrayA);

// Declaracion de un array con el constructor Array()
const arrayB = new Array();
console.log(arrayB);

// Un arreglo puede tener diferentes tipos de datos y dentro de un array puede haber mas arrays
const arrayC = [1, true, 'Hola', ['A', 'B', 'C', [1, 2, 3, 4, 5]]];
console.log(arrayC);

const arrayD = new Array(1, true, 'Hola');
console.log(arrayD);

// Accediendo a una posicion de un array
console.log(arrayC[2]);
console.log(arrayC[3]);

// Accediendo a una posicion de un array dentro de un array
console.log(arrayC[3][1]);
console.log(arrayC[3][3][2]);

// La propiedad "length" nos permite conocer el tamaño de un array
console.log(arrayC.length);
console.log(arrayD.length);

// El metodo "Array.of()" crea un nuevo array con elementos pasados como argumento
const arrayE = Array.of('x', 'y', 'z', 1, 2, 3);
console.log(arrayE);

// El metodo "fill()" cambia todos los elementos en un arreglo por un valor estatico
const arrayF = Array(50).fill(1);
console.log(arrayF);

// El metodo "forEach()" ejecuta la funcion indicada una vez por cada elemento del array
const vocales = ['a', 'e', 'i', 'o', 'u'];

vocales.forEach(function (value, index) {
  console.log(`<li id="${index}">${value}</li>`);
});

const marcasCelulares = ['xiaomi', 'sony', 'lg', 'samsung', 'nokia'];

marcasCelulares.forEach(function (value, index) {
  console.log(`La posicion ${index} es ${value}`);
});

// El metodo "push()" añade uno o mas elementos al final del array
const colores = ['rojo', 'verde', 'azul'];
console.log(colores);

colores.push('amarillo', 'negro', 'blanco');
console.log(colores);

// El metodo "unshift()" añade uno o mas elementos al inicio del array
const colores2 = ['rojo', 'verde', 'azul'];
console.log(colores2);

colores2.unshift('amarillo', 'negro', 'blanco');
console.log(colores2);

// El metodo "pop()" elimina el ultimo elemento de un array
const verduras = ['brocoli', 'ejote', 'chile', 'romana', 'rabano'];
console.log(verduras);

verduras.pop();
console.log(verduras);

// El metodo "shift()" elimina el primer elemento de un array
const marcasAutos = ['bmw', 'audi', 'mercedes benz', 'toyota'];
console.log(marcasAutos);

marcasAutos.shift();
console.log(marcasAutos);

// El metodo "reverse()" invierte el orden de los elementos de un array
const frutas = ['coco', 'manzana', 'pera', 'durazno', 'sandia'];
console.log(frutas);

frutas.reverse();
console.log(frutas);

// El metodo "sort()" ordena los elementos del arreglo y devuelve el arreglo ordenado
const hierbas = ['manzanilla', 'espinaca', 'cebollin', 'perejil', 'acelga', 'cilantro'];
console.log(hierbas);

hierbas.sort();
console.log(hierbas);

const valoresNumericos = [5, 4, 3, 2, 1, 0];
console.log(valoresNumericos);
console.log(valoresNumericos.sort());