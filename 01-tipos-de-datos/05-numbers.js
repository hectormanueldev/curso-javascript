// Declaracion de un numero usando let
let num1 = 2;
console.log(num1);

// Declaracion de un numero usando el constructor Number()
let num2 = new Number(2);
console.log(num2);

// El metodo "toFixed()" formatea un numero usando notacion de punto fijo
let num3 = 7.19;
console.log(num3.toFixed(1));
console.log(num3.toFixed(2));

// El metodo "parseInt()" analiza un argumento de cadena y devuelve un entero
let num4 = 8.96;
console.log(parseInt(num4));
console.log(Number.parseInt(num4));

let num5 = '8.96';
console.log(parseInt(num5));
console.log(Number.parseInt(num5));

console.log(num4 + parseInt(num5));
console.log(num4 + Number.parseInt(num5));

// El metodo "parseFloat()" parsea un argumento cadena y regresa un numero de punto flotante
let num6 = 8.96;
console.log(parseFloat(num6));
console.log(Number.parseFloat(num6));

let num7 = '8.96';
console.log(parseFloat(num7));
console.log(Number.parseFloat(num7));

console.log(num6 + parseFloat(num7));
console.log(num6 + Number.parseFloat(num7));

// El metodo "toString()" devuelve una cadena que representa al objeto
let num8 = 2950;
console.log(num8.toString());

// El metodo "valueOf()" retorna el valor primitivo del objeto especificado
let numObj = new Number(100);
console.log(numObj);
console.log(typeof numObj);

let num9 = numObj.valueOf();
console.log(num9);
console.log(typeof num9);