/*
  - Una funcion es un bloque de codigo autocontenido que se puede definir una vez y ejecutar en cualquier momento, opcionalmente una funcion puede aceptar parametros y devolver un valor
  - Se dice que las funciones son ciudadanos de primera clase porque pueden asignarse a un valor, y pueden pasarse como argumentos y usarse como un valor de retorno
*/

// Declaracion de una funcion
function estoEsUnaFuncion() {
  console.log('Uno');
  console.log('Dos');
  console.log('Tres');
};

// Invocacion de una funcion
estoEsUnaFuncion();

// Funcion que retorna un valor
function unaFuncionQueDevuelveValor() {
  console.log('Uno');
  return 19;
  console.log('Dos');
  return 'La funcion ha retornado una cadena de texto';
};

let valorDeFuncion = unaFuncionQueDevuelveValor();
console.log(valorDeFuncion);

// Funcion que recibe valores
function saludar(nombre, edad) {
  console.log(`Hola mi nombre es ${nombre} y tengo ${edad} años`);
};

saludar('Hector Manuel', 21);

function saludar2(nombre = 'Hector Manuel', edad = 21) {
  console.log(`Hola mi nombre es ${nombre} y tengo ${edad} años`);
};

saludar2();

// Funciones declaradas
funcionDeclarada();

function funcionDeclarada() {
  console.log('Esto es una funcion declarada, puede invocarse en cualquier parte de nuestro codigo, incluso antes de que la funcion sea declarada');
};

// Funciones expresadas o anonimas
const funcionExpresada = function () {
  console.log('Esto es una funcion expresada, es decir, una funcion que se le ha asignado como valor a una variable, si invocamos esta funcion antes de su definicion, JavaScript nos dira ..........');
};

funcionExpresada();

// Ejemplos de funciones que reciben parametros
const square = function (num) {
  return num * num;
};

console.log(square(10));

let multiplicacion = square(10);
console.log(multiplicacion);

const square2 = function (num = 20) {
  return num * num;
};

console.log(square2());

let multi = square2();
console.log(multi);