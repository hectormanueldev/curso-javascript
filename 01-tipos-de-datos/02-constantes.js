/*
  - Presentan un ambito de bloque como let
  - No se puede declarar una costante con el mismo nombre que una funcion o una variable en el mismo ambito
  - Usaremos const cuando una variable no cambiará su valor
  - Si usamos const en datos primitivos, no se puede cambiar el valor de la constante
  - Si usamos const en datos compuestos, si podremos modificar el valor, ya que no estamos accediendo al valor si no a la referencia del valor
*/

const PI = 3.1416;
console.log(PI);

let perro;
perro = "Pakita";
console.log(perro);

// Objeto usando let
let informacionPersona = {
  nombre: "Hector",
  edad: 21,
};

console.log(informacionPersona);

informacionPersona.correo = "hectormanueldev@gmail.com";
console.log(informacionPersona);

// Objeto usando const
let informacionAnimal = {
  nombre: "Pakita",
  edad: 3,
};

console.log(informacionAnimal);

informacionAnimal.raza = "corriente";
console.log(informacionAnimal);

// Array usando let
let colores = ["Blanco", "Negro", "Azul"];
console.log(colores);

colores.push("Amarillo");
console.log(colores);

// Array usando const
const frutas = ["Limon", "Sandia", "Melon"];
console.log(frutas);

frutas.push("Platano");
console.log(frutas);