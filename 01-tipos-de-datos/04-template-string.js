let nombre = 'Hector Manuel';
let apellido = 'Santos';

// Concatenacion
let saludo = 'Hola mi nombre es ' + nombre + ' ' + apellido;
console.log(saludo);

// Interpolacion de variables
let saludo2 = `Hola mi nombre es ${nombre} ${apellido}`;
console.log(saludo2);

// Ejemplo de interpolacion de variables
let a = 5;
let b = 10;

console.log(`Quince es: ${a + b} y no ${2 * a + b}`);

let ul = `
  <ul>
    <li>Primavera</li>
    <li>Verano</li>
    <li>Otoño</li>
    <li>Invierno</li>
  </ul>
`;

console.log(ul);