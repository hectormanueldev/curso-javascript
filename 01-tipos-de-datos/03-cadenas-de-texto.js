// Manera primitiva de representar las cadenas de texto
let cadena1 = "Una cadena primitiva";
console.log(cadena1);

let cadena2 = 'Tambien una cadena primitiva';
console.log(cadena2);

let cadena3 = `Otra cadena primitiva mas`;
console.log(cadena3);

let cadena4 = '     Lorem, ipsum dolor sit amet consectetur adipisicing elit';
console.log(cadena4);

// Manera usando el constructor String()
let cadena5 = new String('Un objeto String');
console.log(cadena5);

// La propiedad "length" nos permite conocer el tamaño de una cadena de texto
console.log(cadena1.length);
console.log(cadena2.length);
console.log(cadena3.length);

// El metodo "toUpperCase()" devuelve el valor de la cadena convertido a mayusculas
console.log(cadena1.toUpperCase());

// El metodo "toLowerCase()" devuelve el valor de la cadena convertido a minusculas
console.log(cadena2.toLowerCase());

// El metodo "includes()" determina si la cadena contiene la palabra que se busca
console.log(cadena4.includes('dolor'));
console.log(cadena4.includes('hector'));

// El metodo "trim()" recorta los espacios en blanco en el principio y en el fin de la cadena
console.log(cadena4);
console.log(cadena4.trim());

// El metodo "split()" devuelve una cadena de texto en un arreglo
console.log(cadena5.split());
console.log(cadena5.split(' '));

// El metodo "slice()" extrae una seccion de una cadena y devuelve una nueva cadena
console.log(cadena2.slice());
console.log(cadena2.slice(8, 18));