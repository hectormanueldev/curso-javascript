// Variables de ambito global
var musica = "Rock";
console.log("Variable musica antes del bloque: ", musica);

{
  var musica = "Pop";
  console.log("Variable musica en el bloque: ", musica);
}

console.log("Variable musica despues del bloque: ", musica);

// Variables de ambito de bloque
let music = "Rock";
console.log("Variable music antes del bloque: ", music);

{
  let music = "Pop";
  console.log("Variable music en el bloque: ", music);
}

console.log("Variable music despues del bloque: ", music);