// La propiedad global undefined representa a una variable a la que no se le ha asignado valor, o no se ha declarado en absoluto
let indefinida;
console.log(indefinida);

// El valor null representa intencionalmente un valor nulo o vacio
let nulo = null;
console.log(nulo);

// La propiedad global NaN es un valor que representa (Not A Number)
let nan = 'Esto no es un NaN';
console.log(parseInt(nan));