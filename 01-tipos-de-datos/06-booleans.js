// Declaracion de un booleano usando let
let verdadero = true;
console.log(verdadero, typeof verdadero);

let falso = false;
console.log(falso, typeof falso);

// Declaracion de un booleano usando el constructor Boolean()
let v = new Boolean(true);
console.log(v, typeof v);

let f = new Boolean(false);
console.log(f, typeof f);

// Creacion de objetos Boolean con un valor inicial de false, ver tambien falsy
let bNoParam = new Boolean();
console.log(bNoParam);

let bZero = new Boolean(0);
console.log(bZero);

let bNull = new Boolean(null);
console.log(bNull);

let bEmptyString = new Boolean('');
console.log(bEmptyString);

let bFalse = new Boolean(false);
console.log(bFalse);

// Creacion de objetos Boolean con un valor inicial de true, ver tambien truthy
let bTrue = new Boolean(true);
console.log(bTrue);

let bTrueString = new Boolean('true');
console.log(bTrueString);

let bFalseString = new Boolean('false');
console.log(bFalseString);

let bSuLin = new Boolean('Su Lin');
console.log(bSuLin);

let bArrayProto = new Boolean([]);
console.log(bArrayProto);

let bObjProto = new Boolean({});
console.log(bObjProto);