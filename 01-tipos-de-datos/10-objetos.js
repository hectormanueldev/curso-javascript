// Declaracion de un objeto usando la sintaxis literal del objeto
const a = {};
console.log(a);

// Declaracion de un objeto usando el constructor Object()
const b = new Object();
console.log(b);

/*
  - Dentro de un objeto a las variables se le van a llamar atributos/propiedades y a las funciones se le va a llamar metodos
  - Cuando queramos usar una propiedad dentro del objeto podemos utilizar la palabra 'this'
*/

const hector = {
  nombre: 'hector manuel',
  apellido: 'santos bautista',
  edad: 20,
  pasatiempos: ['caminar', 'ver peliculas', 'escuchar musica', 'programar'],
  soltero: true,
  contacto: {
    email: 'hectormanueldev@gmail.com',
    telefono: '9999999999'
  },
  saludar: function () {
    console.log('Hola :3');
  },
  decirMiNombre: function () {
    console.log(`Hola me llamo ${this.nombre} ${this.apellido}, tengo ${this.edad} años. Mi pasatiempo favorito es ${this.pasatiempos[3]} y me puedes contactar por medio de mi correo ${this.contacto.email}`);
  }
};

console.log(hector);

// Accediendo a la referencia del objeto
console.log(hector['nombre']);
console.log(hector.nombre);
console.log(hector.apellido);
console.log(hector.edad);
console.log(hector.pasatiempos);
console.log(hector.pasatiempos[3]);
console.log(hector.soltero);
console.log(hector.contacto);
console.log(hector.contacto.email);
hector.saludar();
hector.decirMiNombre();

// El metodo "keys()" devuelve un array de las propiedades de un objeto
console.log(Object.keys(hector));

// El metodo "values()" devuelve un array con los valores de las propiedades de un objeto
console.log(Object.values(hector));

// El metodo "hasOwnProperty()" devuelve un booleano indicando si el objeto tiene la propiedad especificada
console.log(hector.hasOwnProperty('nombre'));
console.log(hector.hasOwnProperty('ciudad'));

// El metodo "freeze()" congela un objeto, impide que se le agreguen nuevas propiedades, impide que se eliminen propiedades ya existentes, impide que se pueda modificar su prototipo
const perro = {
  namePerro: 'Pakita'
};

Object.freeze(perro);

perro.namePerro = 'Guardian';
console.log(perro.namePerro);

// El metodo "isFrozen()" determina si un objeto esta congelado
console.log(Object.isFrozen(perro));

// El metodo "seal()" sella un objeto, previniendo que puedan añadirse nuevas propiedades
const criptomonedad = {
  bitcoin: '845,908',
  ether: '20,526',
  litecoin: '2,545'
};

Object.seal(criptomonedad);

// El metodo "isSealed()" devuelve si el objeto esta sellado
console.log(Object.isSealed(criptomonedad));