const d = document;
const $form = d.querySelector('.crud-form');
const $title = d.querySelector('.crud-title');
const $table = d.querySelector('.crud-table');
const $template = d.getElementById('crud-template').content;
const $fragment = d.createDocumentFragment();

const getAll = async () => {
  try {
    let res = await fetch('http://localhost:3000/comedia');
    let json = await res.json();

    if (!res.ok) throw { status: res.status, statusText: res.statusText};

    json.forEach((el) => {
      $template.querySelector('.name').textContent = el.nombre;
      $template.querySelector('.author').textContent = el.autor;
      $template.querySelector('.edit').dataset.id = el.id;
      $template.querySelector('.edit').dataset.name = el.nombre;
      $template.querySelector('.edit').dataset.author = el.autor;
      $template.querySelector('.delete').dataset.id = el.id;

      let $clone = d.importNode($template, true);
      $fragment.appendChild($clone);
    });

    $table.querySelector('tbody').appendChild($fragment);
  } catch (err) {
    let message = err.statusText || 'Ocurrio un Error';
    $form.insertAdjacentHTML('afterend', `<p><b>Error ${err.status}: ${message}</b></p>`);
  }
};

d.addEventListener('DOMContentLoaded', getAll);

d.addEventListener('submit', async (e) => {
  if (e.target === $form) {
    e.preventDefault();

    if (!e.target.id.value) {
      // POST - CREATE
      try {
        let options = {
          method: 'POST',
          headers: {
            'Content-type': 'application/json; charset=utf-8'
          },
          body: JSON.stringify({
            nombre: e.target.nombre.value,
            autor: e.target.autor.value
          })
        };
        let res = await fetch('http://localhost:3000/comedia', options);
        let json = await res.json();

        if (!res.ok) throw { status: res.status, statusText: res.statusText};

        location.reload();
      } catch (err) {
        let message = err.statusText || 'Ocurrio un Error';
        $form.insertAdjacentHTML('afterend', `<p><b>Error ${err.status}: ${message}</b></p>`);
      }
    } else {
      // PUT - UPDATE
      try {
        let options = {
          method: 'PUT',
          headers: {
            'Content-type': 'application/json; charset=utf-8'
          },
          body: JSON.stringify({
            nombre: e.target.nombre.value,
            autor: e.target.autor.value
          })
        };
        let res = await fetch(`http://localhost:3000/comedia/${e.target.id.value}`, options);
        let json = await res.json();

        if (!res.ok) throw { status: res.status, statusText: res.statusText};

        location.reload();
      } catch (err) {
        let message = err.statusText || 'Ocurrio un Error';
        $form.insertAdjacentHTML('afterend', `<p><b>Error ${err.status}: ${message}</b></p>`);
      }
    }
  }
});

d.addEventListener('click', async (e) => {
  if (e.target.matches('.edit')) {
    $title.textContent = 'Editar Libro';
    $form.nombre.value = e.target.dataset.name;
    $form.autor.value = e.target.dataset.author;
    $form.id.value = e.target.dataset.id;
  }

  if (e.target.matches('.delete')) {
    let isDelete = confirm(`¿Estás seguro de eliminar el id ${e.target.dataset.id}?`);

    if (isDelete) {
      // DELETE - DELETE
      try {
        let options = {
          method: 'DELETE',
          headers: {
            'Content-type': 'application/json; charset=utf-8'
          }
        };
        let res = await fetch(`http://localhost:3000/comedia/${e.target.dataset.id}`, options);
        let json = await res.json();

        if (!res.ok) throw { status: res.status, statusText: res.statusText};

        location.reload();
      } catch (err) {
        let message = err.statusText || 'Ocurrio un Error';
        alert(`Error ${err.status}: ${message}`);
      }
    }
  }
});