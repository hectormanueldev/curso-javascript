const d = document;
const $form = d.querySelector('.crud-form');
const $title = d.querySelector('.crud-title');
const $table = d.querySelector('.crud-table');
const $template = d.getElementById('crud-template').content;
const $fragment = d.createDocumentFragment();

const ajax = (options) => {
  let {url, method, success, error, data} = options;
  const xhr = new XMLHttpRequest();

  xhr.addEventListener('readystatechange', (e) => {
    if (xhr.readyState !== 4) return;

    if (xhr.status >= 200 && xhr.status <= 300) {
      let json = JSON.parse(xhr.responseText);
      success(json);
    } else {
      let message = xhr.statusText || 'Ocurrio un Error';
      error(message);
    }
  });
  xhr.open(method || 'GET', url);
  xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
  xhr.send(JSON.stringify(data));
};

const getAll = () => {
  ajax({
    url: 'http://localhost:3000/romance',
    success: (res) => {
      res.forEach((el) => {
        $template.querySelector('.name').textContent = el.nombre;
        $template.querySelector('.author').textContent = el.autor;
        $template.querySelector('.edit').dataset.id = el.id;
        $template.querySelector('.edit').dataset.name = el.nombre;
        $template.querySelector('.edit').dataset.author = el.autor;
        $template.querySelector('.delete').dataset.id = el.id;

        let $clone = d.importNode($template, true);
        $fragment.appendChild($clone);
      });

      $table.querySelector('tbody').appendChild($fragment);
    },
    error: (err) => {
      $table.insertAdjacentHTML('afterend', `<p><b>${err}</b></p>`);
    }
  });
};

d.addEventListener('DOMContentLoaded', getAll);

d.addEventListener('submit', (e) => {
  if (e.target === $form) {
    e.preventDefault();

    if (!e.target.id.value) {
      // POST - CREATE
      ajax({
        url: 'http://localhost:3000/romance',
        method: 'POST',
        success: (res) => location.reload(),
        error: (err) => $form.insertAdjacentHTML('afterend', `<p><b>${err}</b></p>`),
        data: {
          nombre: e.target.nombre.value,
          autor: e.target.autor.value
        }
      });
    } else {
      // PUT - UPDATE
      ajax({
        url: `http://localhost:3000/romance/${e.target.id.value}`,
        method: 'PUT',
        success: (res) => location.reload(),
        error: (err) => $form.insertAdjacentHTML('afterend', `<p><b>${err}</b></p>`),
        data: {
          nombre: e.target.nombre.value,
          autor: e.target.autor.value
        }
      });
    }
  }
});

d.addEventListener('click', (e) => {
  if (e.target.matches('.edit')) {
    $title.textContent = 'Editar Libro';
    $form.nombre.value = e.target.dataset.name;
    $form.autor.value = e.target.dataset.author;
    $form.id.value = e.target.dataset.id;
  }

  if (e.target.matches('.delete')) {
    let isDelete = confirm(`¿Estás seguro de eliminar el id ${e.target.dataset.id}?`);

    if (isDelete) {
      // DELETE - DELETE
      ajax({
        url: `http://localhost:3000/romance/${e.target.dataset.id}`,
        method: 'DELETE',
        success: (res) => location.reload(),
        error: () => alert(err)
      });
    }
  }
});