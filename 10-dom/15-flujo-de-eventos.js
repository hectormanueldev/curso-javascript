const $divsEventos = document.querySelectorAll('.eventos-flujo div');
console.log($divsEventos);

function flujoEventos(e) {
  console.log(`Hola te saluda ${this.className}, el click lo origino ${e.target.className}`);
};

$divsEventos.forEach((div) => {
  // Fase de Burbuja, sin el tercer parametro o poniendo explicitamente con false
  // div.addEventListener('click', flujoEventos);
  // div.addEventListener('click', flujoEventos, false);

  // Fase de Captura
  // div.addEventListener('click', flujoEventos, true);

  div.addEventListener('click', flujoEventos, {
    capture: true,
    once: true
  });
});