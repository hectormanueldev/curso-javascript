console.log(document.documentElement.lang);
console.log(document.documentElement.getAttribute('lang'));
console.log(document.querySelector('.link-dom').href);
console.log(document.querySelector('.link-dom').getAttribute('href'));

// Establecer un nuevo valor a los atributos
document.documentElement.lang = 'es';
console.log(document.documentElement.getAttribute('lang'));

document.documentElement.setAttribute('lang', 'es-MX');
console.log(document.documentElement.getAttribute('lang'));

// Guardando en variables
const $linkDOM = document.querySelector('.link-dom');
$linkDOM.setAttribute('target', '_blank');
$linkDOM.setAttribute('rel', 'noopener');
$linkDOM.setAttribute('href', 'https://www.google.com');

// Remover atributos
console.log($linkDOM.hasAttribute('rel'));
$linkDOM.removeAttribute('rel');
console.log($linkDOM.hasAttribute('rel'));

// data-attributes
console.log($linkDOM.getAttribute('data-description'));
console.log($linkDOM.dataset);
console.log($linkDOM.dataset.description);

$linkDOM.setAttribute('data-description', 'Modelo de Objeto del Documento');
console.log($linkDOM.getAttribute('data-description'));

$linkDOM.dataset.description = 'Entra a Aprender Javascript para mas conocimiento';
console.log($linkDOM.dataset.description);

console.log($linkDOM.hasAttribute('data-id'));
$linkDOM.removeAttribute('data-id');
console.log($linkDOM.hasAttribute('data-id'));