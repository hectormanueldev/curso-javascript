const $card = document.querySelector('.card');

console.log($card);
console.log($card.className);
console.log($card.classList);

// Agregar una clase
console.log($card.classList.contains('rotate-45'));
$card.classList.add('rotate-45');
console.log($card.classList.contains('rotate-45'));
console.log($card.className);
console.log($card.classList);

// Remover una clase
$card.classList.remove('rotate-45');
console.log($card.classList.contains('rotate-45'));
console.log($card.className);
console.log($card.classList);

// Para poner clases si es que no la tienen, y quitar la clase si la tiene
$card.classList.toggle('rotate-45');
console.log($card.classList.contains('rotate-45'));

$card.classList.toggle('rotate-45');
console.log($card.classList.contains('rotate-45'));

$card.classList.toggle('rotate-45');
console.log($card.classList.contains('rotate-45'));

// Reemplazar una clase
$card.classList.replace('rotate-45', 'rotate-135');

// Agregar varias clases a la vez
$card.classList.add('opacity-80', 'sepia');
$card.classList.remove('opacity-80', 'sepia');
$card.classList.toggle('opacity-80', 'sepia');