window.addEventListener('resize', (e) => {
  console.clear();
  console.log('********** Evento Resize **********');
  console.log(window.innerWidth);
  console.log(window.innerHeight);
  console.log(window.outerWidth);
  console.log(window.outerWidth);
});

window.addEventListener('scroll', (e) => {
  console.clear();
  console.log('********** Evento Scroll **********');
  console.log(window.scrollX);
  console.log(window.scrollY);
});

window.addEventListener('load', (e) => {
  console.log('********** Evento Load **********');
  console.log(window.screenX);
  console.log(window.screenY);
});

document.addEventListener('DOMContentLoaded', (e) => {
  console.log('********** DOMContentLoaded **********');
  console.log(window.screenX);
  console.log(window.screenY);
});