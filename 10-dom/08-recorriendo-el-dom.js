const $cards = document.querySelector('.cards');

console.log($cards);
console.log($cards.children);
console.log($cards.children[2]);
console.log($cards.parentElement);

// Para ver los elementos primeros y finales
console.log($cards.firstElementChild);
console.log($cards.lastElementChild);

// Para ver los nodos anteriores y los que siguen
console.log($cards.previousElementSibling);
console.log($cards.nextElementSibling);

// El metodo "closest()" devuelve el ascendiente mas cercano al elemento actual que coincida con el selector proporcionado por parámetro
console.log($cards.closest('div'));
console.log($cards.closest('body'));
console.log($cards.children[3].closest('section'));