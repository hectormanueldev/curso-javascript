const $linkDOM = document.querySelector('.link-dom');

// Obtener los estilos de nuestro css
console.log($linkDOM.style);
console.log($linkDOM.getAttribute('style'));
console.log($linkDOM.style.backgroundColor);
console.log($linkDOM.style.color);
console.log(window.getComputedStyle($linkDOM));
console.log(getComputedStyle($linkDOM).getPropertyValue('color'));

// Establecer estilos al css
$linkDOM.style.setProperty('text-decoration', 'none');
$linkDOM.style.setProperty('display', 'block');
$linkDOM.style.setProperty('width', '50%');
$linkDOM.style.fontSize = '40px';
$linkDOM.style.textAlign = 'center';
$linkDOM.style.textTransform = 'LowerCamelCase';
$linkDOM.style.margin = '0 auto';
$linkDOM.style.borderRadius = '8px';
console.log($linkDOM.style);
console.log($linkDOM.getAttribute('style'));

// Variable css custom-properties
const $html = document.documentElement;
const $body = document.body;

let varDarkColor = getComputedStyle($html).getPropertyValue('--dark-color');
let varYellowColor = getComputedStyle($body).getPropertyValue('--yellow-color');

console.log(varDarkColor);
console.log(varYellowColor);

$body.style.backgroundColor = varDarkColor;
$body.style.color = varYellowColor;

$html.style.setProperty('--dark-color', '#000');

varDarkColor = getComputedStyle($html).getPropertyValue('--dark-color');
$body.style.backgroundColor = varDarkColor;