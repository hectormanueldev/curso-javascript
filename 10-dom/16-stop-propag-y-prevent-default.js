const $divsEventos = document.querySelectorAll('.eventos-flujo div');
const $linkEventos = document.querySelector('.eventos-flujo a');

function flujoEventos(e) {
  console.log(`Hola te saluda ${this.className}, el click lo origino ${e.target.className}`);
  e.stopPropagation();
};

console.log($divsEventos);

$divsEventos.forEach((div) => {
  // Fase de Burbuja
  div.addEventListener('click', flujoEventos);
});

$linkEventos.addEventListener('click', (e) => {
  alert('Hola soy tu amigo y docente digital jonathan mircha');
  e.preventDefault();
  e.stopPropagation();
});