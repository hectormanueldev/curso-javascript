const $whatIsDOM = document.getElementById('que-es');

let text = `
  <p>
    El Modelo de Objetos del Documento (<b><i>DOM - Document Object Model</i></b>) es una API para documentos HTML y XML
  </p>
  <p>
    Este proveé una representacion del documento, permitiendo modificar su contenido y representacion visual mediante codigo JS
  </p>
  <p>
    <mark>El DOM no es parte de la especificacion de JavaScript, es una API para los navegadores</mark>
  </p>
`;

$whatIsDOM.textContent = text;
$whatIsDOM.innerHTML = text;
$whatIsDOM.outerHTML = text;