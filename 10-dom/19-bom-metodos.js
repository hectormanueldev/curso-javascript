const $btnAbrir = document.getElementById('abrir-ventana');
const $btnCerrar = document.getElementById('cerrar-ventana');
const $btnImprimir = document.getElementById('imprimir-ventana');

let ventanaAbierta;

$btnAbrir.addEventListener('click', (e) => {
  // open('https://jonmircha.com');
  ventanaAbierta = open('https://jonmircha.com');
});

$btnCerrar.addEventListener('click', (e) => {
  // close();
  ventanaAbierta.close();
});

$btnImprimir.addEventListener('click', (e) => {
  print();
});