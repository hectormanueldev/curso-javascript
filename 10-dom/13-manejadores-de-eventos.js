function holaMundo() {
  alert('Hola Mundo');
  console.log(event);
};

// Asignar un solo evento al boton
const $eventoSemantico = document.getElementById('evento-semantico');

$eventoSemantico.onclick = holaMundo;

$eventoSemantico.onclick = (e) => {
  alert('Hola mundo manejador de eventos semantico');
  console.log(e);
};

// Asignar eventos multiples a un boton
const $eventoMultiple = document.getElementById('evento-multiple');

$eventoMultiple.addEventListener('click', holaMundo);

$eventoMultiple.addEventListener('click', (e) => {
  alert('Hola mundo manejador de eventos multiples');
  console.log(e);
  console.log(e.type);
  console.log(e.target);
});