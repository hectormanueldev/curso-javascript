function holaMundo() {
  alert('Hola Mundo');
  console.log(event);
};

function saludar(nombre = 'Desconocido') {
  alert(`Hola ${nombre}`);
};

const $eventoMultiple = document.getElementById('evento-multiple');

$eventoMultiple.addEventListener('click', holaMundo);

$eventoMultiple.addEventListener('click', (e) => {
  alert('Hola mundo manejador de eventos multiples');
  console.log(e);
  console.log(e.type);
  console.log(e.target);
});

// Para pasar parametros en una funcion, tenemos que envolver en una arrow function nuestra funcion
$eventoMultiple.addEventListener('click', () => {
  saludar();
  saludar('Hector');
});

// Remover elementos
const removerDobleClick = (e) => {
  alert(`Removiendo el evento de tipo ${e.type}`);
  console.log(e);
  $eventoRemover.removeEventListener('dblclick', removerDobleClick);
  $eventoRemover.disabled = true;
};

const $eventoRemover = document.getElementById('evento-remover');

$eventoRemover.addEventListener('dblclick', removerDobleClick);