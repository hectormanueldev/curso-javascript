/*
  location: representa la ubicación (URL) del objeto al que esta vinculado
*/

console.log('********** Objeto URL (location) **********');
console.log(location);

// La propiedad "origin" contiene la serialización Unicode del origen de la URL que representa
console.log(location.origin);

// La propiedad "protocol" representación del esquema de protocolo de la URL
console.log(location.protocol);

// La propiedad "host" contiene el host, es decir, el nombre del host y el puerto de la URL
console.log(location.host);

// La propiedad "hostname" contiene el dominio de la URL
console.log(location.hostname);

// La propiedad "port" contiene el número de puerto de la URL
console.log(location.port);

// La propiedad "href" contiene la URL completa y permite actualizar el href
console.log(location.href);

// La propiedad "hash" contiene un '#' seguido del identificador de fragmento de la URL
console.log(location.hash);

// La propiedad "pathname" contiene una inicial '/'seguida de la ruta de la URL (o la cadena vacía si no hay ruta)
console.log(location.pathname);

// El metodo "reload()" carga de nuevo la URL actual
console.log(location.reload());

/*
  history: permite la manipulación del historial de sesiones del navegador
*/

console.log('********** Objeto Historial (history) **********');
console.log(history);

// La propiedad "length" retorna un entero representando el número de elementos en el historial de la sesión, incluyendo la página cargada actualmente
console.log(history.length);

// La propiedad "state" devuelve un valor que representa el estado en la parte superior de la pila del historial
console.log(history.state);

// El metodo "back()" hace que el navegador retroceda una página en el historial de la sesion
console.log(history.back());

// El metodo "forward()" hace que el navegador avance una página en el historial de la sesion
console.log(history.forward());

// El metodo "go()" carga una página específica del historial de la sesión. Puede usarlo para avanzar y retroceder a través del historial según el valor de un parámetro
console.log(history.go(2));

/*
  navigator: representa el estado y la identidad del user agent
*/

console.log('********** Objeto Navegador (navigator) **********');
console.log(navigator);

// La propiedad "connection" devuelve un objeto que contiene información sobre la conexión del sistema como el ancho de banda actual del dispositivo del usuario o si la conexión está medida
console.log(navigator.connection);

// La propiedad "geolocation" devuelve un objeto que da acceso al contenido web a la ubicación del dispositivo.
console.log(navigator.geolocation);

// La propiedad "mediaDevices" que proporciona acceso a dispositivos de entrada de medios conectados, como cámaras y micrófonos, así como para compartir pantalla
console.log(navigator.mediaDevices);

// La propiedad "mimeTypes" son los tipos de formato que acepta el navegador
console.log(navigator.mimeTypes);

// La propiedad "onLine" devuelve el estado en línea del navegador
console.log(navigator.onLine);

// La propiedad "serviceWorker" devuelve el objeto del documento que proporciona acceso al registro, la eliminación, la actualización y la comunicación
console.log(navigator.serviceWorker);

// La propiedad "storage" utilizado para acceder a las capacidades de almacenamiento generales del navegador
console.log(navigator.storage);

// La propiedad "usb" capacidad de detectar dispostivos usb
console.log(navigator.usb);

// La propiedad "userAgent" devuelve la cadena de agente de usuario para el navegador actual
console.log(navigator.userAgent);