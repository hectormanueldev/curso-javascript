// if else
let edad = 20;

if (edad > 17) {
  console.log('Eres mayor de edad');
} else {
  console.log('Eres menor de edad');
}

if (edad >= 18) {
  console.log('Eres mayor de edad');
} else {
  console.log('Eres menor de edad');
}

if (edad < 18) {
  console.log('Eres menor de edad');
} else {
  console.log('Eres mayor de edad');
}

if (edad <= 17) {
  console.log('Eres menor de edad');
} else {
  console.log('Eres mayor de edad');
}

// if else if
let hora = 21;

if(hora >= 0 && hora <= 5) {
  console.log('Buenas madrugadas');
} else if (hora >= 6 && hora <= 12) {
  console.log('Buenos dias');
} else if (hora >= 13 && hora <= 18) {
  console.log('Buenas tardes');
} else {
  console.log('Buenas noches');
}

// Operador ternario
let eresMayor = edad >= 18 ? 'Eres mayor de edad' : 'Eres menor de edad';
console.log(eresMayor);

let numero = 5;

let numeroLetra =
numero === 0 ? 'cero' :
numero === 1 ? 'uno' :
numero === 2 ? 'dos' :
numero === 3 ? 'tres' :
numero === 4 ? 'cuatro' :
numero === 5 ? 'cinco' :
'usted ha ingresado otro numero';

console.log(numeroLetra);

let firstCheck = false;
let secondCheck = false;
let access = firstCheck ? 'acceso denegado' : secondCheck ? 'acceso denegago' : 'acceso concedido';

console.log(access);

let stop = false;
let age = 21;

let accesoEdad = age >= 18
? (alert('OK, puedes continuar'), open('https://www.google.com/'))
: (stop = true, alert('Disculpa, eres menor de edad'));

console.log(accesoEdad);

// switch case
let fruta = 'mangos';

switch (fruta) {
  case 'naranjas':
    console.log(`El kilogramo de ${fruta} cuesta $0.59`);
  break;
  case 'manzanas':
    console.log(`El kilogramo de ${fruta} cuesta $0.32`);
  break;
  case 'platanos':
    console.log(`El kilogramo de ${fruta} cuesta $0.48`);
  break;
  case 'cerezas':
    console.log(`El kilogramo de ${fruta} cuesta $3.00`);
  break;
  case 'mangos':
  case 'papayas':
    console.log(`El kilogramo de ${fruta} cuesta $2.79`);
  break;
  default:
    console.log(`Lo lamentamos, por el momento no disponemos de ${fruta}`);
  break;
}