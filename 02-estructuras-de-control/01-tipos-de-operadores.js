/*
  Operadores aritmeticos:

  - suma '+'
  - resta '-'
  - division '/'
  - multiplicacion '*'
  - modulo(%)
  - potencia '**'
  - parentesis '()'
*/

console.log(2 + 2);
console.log(2 + true);

console.log(5 - 3);
console.log(5 - true);

console.log(12 / 2);
console.log(6 / '3');

console.log(3 * 4);
console.log('3' * 2);

console.log(12 % 5);
console.log(4 % 2);

console.log(3 ** 4);
console.log(10 ** -2);
console.log(2 ** 3 ** 2);
console.log((2 ** 3) ** 2);

console.log(5 + (5 - 10) * 3);

/*
  Operadores relacionales:

  - menor que '<'
  - mayor que '>'
  - menor o igual '<='
  - mayor o igual '>='
*/

console.log(8 < 10);
console.log(4 < 6);

console.log(8 > 10);
console.log(20 > 100);

console.log(8 <= 100);
console.log(10 <= 10);

console.log(8 >= 9);
console.log(9 >= 8);

/*
  Operadores de igualdad:

  - igualdad '=='
  - desigualdad '!='
  - igualdad estricta '==='
  - desigualdad estricta '!=='
*/

console.log(7 == 7);
console.log('7' == 7);
console.log(0 == false);

console.log(7 != 10);
console.log(7 != '10');

console.log(7 === 7);
console.log('7' === 7);
console.log(0 === false);

console.log(7 !== 10);
console.log(7 !== '10');

/*
  Operadores de asignacion
*/

let i = 1;
i += 3;     // i = i + 3;
console.log(i);

let j = 1;
j -= 3;     // j = j - 3;
console.log(j);

let k = 1;
k /= 3;     // k = k / 3;
console.log(k);

let l = 1;
l *= 3;     // l = l * 3;
console.log(l);

/*
  Operadores unarios
*/
let m = 2;
m++;
console.log(m);

let n = 2;
n--;
console.log(n);

// Tener cuidado con este tipo de asignacion
let o = 2;
++o;
console.log(o);

let p = 2;
--p;
console.log(p);

/*
  Operador logico: not '!', lo que es verdadero lo devuelve falso y viceversa
*/
console.log(true);
console.log(!true);
console.log(!false);

/*
  Operador logico: or '||', cuando tenga dos o mas condiciones, con que una se cumpla, el or validara
*/
console.log((9 === 9) || ('9' === 9));
console.log((9 === '9') || ('9' === 9));

/*
  Operador logico: and '&&', cuando tenga dos o mas condiciones, todas tienen que cumplirse
*/
console.log((9 === 9) && ('9' === 9));
console.log((9 ===9) && (9 === 9));