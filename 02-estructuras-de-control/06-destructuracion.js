/*
  La destructuracion es una expresion de JavaScript que permite desempacar valores de arreglos o propiedades de objetos en distintas variables.
*/

// Destructuracion de un arreglo y asignacion basica de variables
const numeros = [1, 2, 3, 4, 5];
const [uno, dos, tres, cuatro, cinco] = numeros;

console.log(uno);
console.log(dos);
console.log(tres);
console.log(cuatro);
console.log(cinco);

// Asignacion separada de la declaracion
let a;
let b;

[a, b] = [1, 2];

console.log(a);
console.log(b);

// Destructuracion de un objeto. Para que ocurra la destructuracion en un objeto es importante que la variable que se crea se llame igual a la propiedad del objeto
const persona = {
  nombre: 'hector',
  apellido: 'santos',
  edad: 20
};

const { nombre, apellido, edad } = persona;

console.log(nombre);
console.log(apellido);
console.log(edad);

// Asignar nuevos nombres de variables en la destructuracion de un objeto
const o = { p: 42, q: true };
const { p: foo, q: bar } = o;

console.log(foo);
console.log(bar);

// Intercambio de variables
let c = 1;
let d = 2;

console.log(c);
console.log(d);

[c, d] = [d, c];

console.log(c);
console.log(d);