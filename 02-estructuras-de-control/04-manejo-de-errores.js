/*
  La declaracion try...catch señala un bloque de instrucciones a intentar (try), y especifica una respuesta si se produce una excepcion (catch)
*/

try {
  console.log('En el try se agrega el codigo a evaluar');
} catch (error) {
  console.log('En el catch se captura cualquier error surgido o lanzado en el try');
} finally {
  console.log('En el finally se ejecutara siempre al final de un bloque try...catch');
}

// Generando un error
try {
  noExisteFuncion();
} catch (error) {
  console.log(error);
}

// Personalizando errores
try {
  let numero = 'Hola';

  if (isNaN(numero)) {
    throw new Error('El caracter introducido no es un numero introducido');
  }

  console.log(numero * numero);
} catch (error) {
  console.log(`Se produjo el siguiente error ${error}`);
}