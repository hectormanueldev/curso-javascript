let nombre = 'hector';
let edad = 20;

// Primer forma de declarar
const miNombre = {
  nombre: nombre,
  edad: edad,
  ladrar: function () {
    console.log('guauu guauu');
  }
};

console.log(miNombre);
miNombre.ladrar();

// Nueva forma de declarar
const myName = {
  nombre,
  edad,
  estatura: 1.60,
  ladrar: function () {
    console.log('guauu guauu guauu');
  }
};

console.log(myName);