/*
  Una expresion de funcion flecha es una alternativa compacta a una expresion de funcion tradicional, pero es limitada y no se puede utilizar en todas las situaciones
*/

const saludar = () => console.log('Hola');

saludar();

// Arrow function que recibe parametros
const saludarPersona = nombre => console.log(`Hola ${nombre}`);

saludarPersona('hector');

// Arrow function que retorna un valor
const sumar = (a, b) => a + b;
console.log(sumar(10, 24));

const restar = (c, d) => c - d;
console.log(restar(24, 10));

// Arrow function de varias lineas
const funcionDeVariasLineas = () => {
  console.log('uno');
  console.log('dos');
  console.log('tres');
};

console.log(funcionDeVariasLineas);

funcionDeVariasLineas();

// Arrow function con metodo forEach()
const numeros = [1, 2, 3, 4, 5];

numeros.forEach(function (value, index) {
  console.log(`${value} esta en el index ${index}`);
});

numeros.forEach((value, index) => {
  console.log(`${value} esta en el index ${index}`);
});

// No usar arrow function en objetos
function Perro() {
  console.log(this);
};

Perro();

const conejo = {
  nombre: 'manuel',
  ladrar() {
    console.log(this);
  }
};

conejo.ladrar();

const conejo2 = {
  nombre: 'manuel',
  ladrar: () => {
    console.log(this);
  }
};

conejo2.ladrar();