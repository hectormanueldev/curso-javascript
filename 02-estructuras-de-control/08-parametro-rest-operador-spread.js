/*
  Parametros rest: La sintaxis de los parametros rest nos permiten representar un numero indefinido de argumentos como un array
*/

function sumar(a, b, ...c) {
  let resultado = a + b;

  c.forEach(function (value) {
    resultado += value;
  });

  return resultado;
};

console.log(sumar(1, 2));
console.log(sumar(1, 2, 3));
console.log(sumar(1, 2, 3, 4));
console.log(sumar(1, 2, 3, 4, 5));
console.log(sumar(1, 2, 3, 4, 5, 6));

/*
  Operador spread: Permite a un elemento iterable tal como un arreglo o cadena ser expandido en lugares donde cero o mas argumentos son esperados, o a un objeto ser expandido en lugares donde cero o mas pares de valores clave son esperados
*/

const array1 = [1, 2, 3, 4, 5];
const array2 = [6, 7, 8, 9, 0];

console.log(array1, array2);

const array3 = [...array1, ...array2];
console.log(array3);