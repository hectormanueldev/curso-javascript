/*
  break: Termina el bucle actual, sentencia switch o label y transfiere el control del programa a la siguiente sentencia de terminacion de estos elementos
*/

const numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];

for (let i = 0; i < numeros.length; i++) {
  if (i === 5) {
    break;
  }

  console.log(numeros[i]);
}

/*
  continue: Termina la ejecucion de las sentencias de la iteracion actual del bucle actual o la etiqueta y continua la ejecucion del bucle con la proxima iteracion
*/

for (let i = 0; i < numeros.length; i++) {
  if (i === 5) {
    continue;
  }

  console.log(numeros[i]);
}