/*
  while: Crea un bucle que ejecuta una sentencia especificada mientras cierta condicion se evalue como verdadera, dicha condicion es evaluada antes de ejecutar la sentencia
*/

let n = 0;
let x = 0;

while (n < 3) {
  n++;
  x += n;
}

console.log(n);
console.log(x);

let contador = 0;

while (contador < 10) {
  console.log(`while ${contador}`);
  contador++;
}

/*
  do while: Crea un bucle que ejecuta una sentencia especificada, hasta que la condicion de comprobacion se evalua como falsa
*/

let i = 0;

do {
  console.log(`do while ${i}`);

  i++;
} while (i < 10);

let j = 0;

do {
  j += 1;

  console.log(j);
} while (j < 5);

/*
  for: Crea un bucle que consiste en tres expresiones opcionales, encerradas en parentesis y separados por punto y coma, seguidos de una sentencia ejecutada en un bucle
*/

for (let i = 0; i < 10; i++) {
  console.log(`for ${i}`);
}

const numeros = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

for (let i = 0; i < numeros.length; i++) {
  console.log(numeros[i]);
}

/*
  for in: Itera sobre todas las propiedades enumerables de un objeto que esta codificado por cadenas incluidas las propiedades enumerables heredadas
*/

const hector = {
  nombre: 'hector',
  apellido: 'santos',
  edad: 20
};

for (const property in hector) {
  console.log(`${property} : ${hector[property]}`);
}

const objNumeros = {
  a: 1,
  b: 2,
  c: 3
};

for (const property in objNumeros) {
  console.log(`${property} : ${objNumeros[property]}`);
}

/*
  for of: Ejecuta un bloque de codigo para cada elemento de un objeto iterable, como lo son: String, Array, objetos similares a Array e iterables definidos por el usuario
*/

const numeros2 = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];

// Usar const si no se va a modificar la variable dentro del bloque
for (const value of numeros2) {
  console.log(value);
}

// Usar let si vamos a modificar la variable dentro del bloque
for (let value of numeros2) {
  value += 10;
  console.log(value);
}

let nombreCompleto = 'hector manuel santos';

for (const value of nombreCompleto) {
  console.log(value);
}