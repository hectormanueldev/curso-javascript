const iterable1 = [1, 2, 3, 4, 5];
const iterable2 = 'Hola Mundo';
const iterable3 = new Set([1, 2, 3, 4, 5, 6, 7, 8, 9]);
const iterable4 = new Map([
  ['nombre', 'hector'],
  ['apellido', 'santos'],
  ['edad', 20]
]);

// Accedemos al iterador del iterable
const iterador = iterable4[Symbol.iterator]();

console.log(iterable4);
console.log(iterador);

let next = iterador.next();

while (!next.done) {
  console.log(next.value);
  next = iterador.next();
}