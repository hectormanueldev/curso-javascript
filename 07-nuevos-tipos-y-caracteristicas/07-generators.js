function* iterable() {
  yield 'hola';
  console.log('Hola consola');
  yield 'hola2';
  console.log('Seguimos con mas instrucciones de nuestro codigo');
  yield 'hola3';
  yield 'hola4';
};

let iterador = iterable();

for (const y of iterador) {
  console.log(y);
}

const arreglo = [...iterable()];
console.log(arreglo);

function cuadrado(valor) {
  setTimeout(() => {
    return console.log({valor, resultado: valor * valor});
  }, Math.random() * 1000);
};

function* generador() {
  console.log('Inicia generador');
  yield cuadrado(0);
  yield cuadrado(1);
  yield cuadrado(2);
  yield cuadrado(3);
  yield cuadrado(4);
  yield cuadrado(5);
  console.log('Fin generador');
};

let gen = generador();

for (const y of gen) {
  console.log(y);
}