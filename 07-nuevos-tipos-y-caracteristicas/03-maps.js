// El objeto Map almacena pares clave/valor. Cualquier valor (tanto objetos como valores primitivos) pueden ser usados como clave o valor
const mapa = new Map();

// El metodo "set()" agrega un nuevo elemento al objeto Map con la llave y el valor especificado
mapa.set('nombre', 'hector');
mapa.set('apellido', 'santos');
mapa.set('edad', 20);

console.log(mapa);

// La propiedad "size" nos devuelve el tamaño del Map
console.log(mapa.size);


// El metodo "has()" devuelve un booleano indicando si el elemento con la llave especificada existe o no
console.log(mapa.has('correo'));
console.log(mapa.has('nombre'));

// El metodo "get()" devuelve un elemento especifico de un objeto Map
console.log(mapa.get('nombre'));
console.log(mapa.get('apellido'));
console.log(mapa.get('edad'));

// El metodo "set()" nos permite cambiar el valor de una propiedad
mapa.set('nombre', 'hector manuel');
mapa.set('apellido', 'santos bautista');

console.log(mapa);

// El metodo "delete()" elimina el elemento especificado de un objeto Map
mapa.delete('edad');
console.log(mapa);

mapa.set(19, 'diecinueve');
mapa.set(false, 'false');
mapa.set({}, {});
console.log(mapa);

// Recorrer un Map
for (let [key, value] of mapa) {
  console.log(`La llave ${key} tiene como valor ${value}`);
}

// Otra forma de agregar propiedades a un Map
const mapa2 = new Map([
  ['nombre', 'hector manuel'],
  ['apellido', 'santos'],
  ['edad', 20],
  [null, 'nulo']
]);

console.log(mapa2);

// Obtener las llaves del Map
const llavesMap2 = [...mapa2.keys()];
console.log(llavesMap2);

// Obtener los valores del Map
const valoresMap2 = [...mapa2.values()];
console.log(valoresMap2);