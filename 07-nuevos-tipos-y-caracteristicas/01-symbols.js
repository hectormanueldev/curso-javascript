// Symbols es un tipo de dato primitivo en cuyos valores son unicos e inmutables
let id1 = Symbol('usuario1');
let id2 = Symbol('usuario1');

console.log(id1, id2);
console.log(typeof id1, typeof id2);
console.log(id1 === id2);

// Creacion de los Symbols vacios
const NOMBRE = Symbol('nombre');
const SALUDAR = Symbol('saludar');

// En un objeto agregamos valores a nuestros Symbol vacios
const persona = {
  [NOMBRE]: 'Melvin Cruickshank Jr.',
  edad: 40
};

// Imprimimos nuestro Symbol
console.log(persona);
console.log(persona[NOMBRE]);

// Crear una funcion con Symbol
persona[SALUDAR] = () => {
  console.log(`Hola me llamo ${persona[NOMBRE]}`);
};

// Invocacion de una funcion tipo Symbol
persona[SALUDAR]();

// Agregando propiedades al objeto con el mismo nombre que el Symbol creado
persona.NOMBRE = 'Janice Graham I';
console.log(persona);
console.log(persona.NOMBRE);

// Recorrer un objeto
for (const key in persona) {
  console.log(key);
  console.log(persona[key]);
}

// El metodo "getOwnPropertySymbols" devuelve una coleccion de todas las propiedades de los simbolos encontrados directamente en un objeto dado
console.log(Object.getOwnPropertySymbols(persona));