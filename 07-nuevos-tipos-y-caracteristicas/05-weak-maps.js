// El objeto WeakMap es una coleccion de pares clave/valor en la que las claves son objetos y los valores son valores arbitrarios
const wm = new WeakMap();

let llave1 = {};
let llave2 = {};
let llave3 = {};

// El metodo "set()" añade un nuevo elemento con su key y value especificos al objeto Weakmap
wm.set(llave1, 1);
wm.set(llave2, 2);
console.log(wm);

// El metodo "get()" devuelve un elemento especifico del objeto Weakmap
console.log(wm.get(llave1));
console.log(wm.get(llave2));
console.log(wm.get(llave3));

// El metodo "delete()" elimina un elemento especifico del objeto Weakmap
wm.delete(llave2);
console.log(wm);

wm.set(llave2, 2);
wm.set(llave3, 3);
console.log(wm);