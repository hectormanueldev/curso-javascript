// Asignando dinamicamente id's a usuarios
let idAleatorio = Math.round(Math.random() * 100 + 5);

const objUsuarios = {
  [`id_${idAleatorio}`]: 'Valor Aleatorio'
};

console.log(objUsuarios);

const listaUsuarios = ['Mona Boehm', 'Damon Olson', 'Lois Flatley', 'Roberto Kling', 'Rodolfo Gutkowski', 'Marion Weissnat', 'Glen Prohaska', 'Ted Kshlerin'];

listaUsuarios.forEach((usuario, index) => objUsuarios[`id_${index}`] = usuario);

console.log(objUsuarios);