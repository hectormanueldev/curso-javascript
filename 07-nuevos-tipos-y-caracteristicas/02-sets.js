// El objeto Set permite almacenar valores unicos de cualquier tipo, incluso valores primitivos y referencias a objetos
const set = new Set([1, 2, 3, 4, 5, true, false, false, {}, {}, 'hola', 'HOLA']);
console.log(set);

// La propiedad "size" devuelve el numero de elementos que hay en el objeto Set
console.log(set.size);

// El metodo "add()" añade un nuevo elemento con un valor especifico al final del objeto Set
const set2 = new Set();

set2.add(1);
set2.add(2) .add(3) .add(4) .add(5);
set2.add(true) .add(false) .add(false) .add({}) .add({}) .add('hola') .add('HOLA');

console.log(set2);
console.log(set2.size);

// Recorriendo set
console.log('***** Recorriendo set *****');

for (const iterator of set) {
  console.log(iterator);
}

// Recorriendo set2
console.log('***** Recorriendo set2 *****');

set.forEach(value => {
  console.log(value);
});

// El metodo "delete()" remueve el elemento especificado del objeto Set
set2.delete(1);
console.log(set2);

// El metodo "has()" retorna un booleano indicando si el elemento especificado existe en el objeto Set o no
console.log(set2.has(1));
console.log(set2.has(2));

// El metodo "clear()" remueve todos los elementos de un objeto set
set2.clear();
console.log(set2);

// Convertir un Set en un arreglo
let array = Array.from(set);

console.log(array);
console.log(array[10]);