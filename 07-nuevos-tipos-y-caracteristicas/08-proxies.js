// Se usa para definir un comportamiento personalizado para operaciones fundamentales (por ejemplo, para observar propiedades, cuando se asignan, enumeracion, invocacion de funciones, etc)
const persona = {
  nombre: '',
  apellido: '',
  edad: 0
};

const manejador = {
  set(obj, prop, value) {
    // Agregando validacion cuando una propiedad no se encuentra en el objeto base
    if (Object.keys(obj).indexOf(prop) === -1) {
      return console.error(`La propiedad ${prop} no existe en el objeto persona`);
    }

    if ((prop === 'nombre' || prop === 'apellido') && !(/^[A-Za-zÑñÁáÉéÍíÓóÚú]+$/g.test(value))) {
      return console.error(`La propiedad ${prop} solo acepta letras y espacios en blanco`);
    }

    obj[prop] = value;
  }
};

const hector = new Proxy(persona, manejador);

hector.nombre = 'hector';
hector.apellido = 'santos';
hector.edad = 20;
hector.telefono = '9999999999';

console.log(hector);