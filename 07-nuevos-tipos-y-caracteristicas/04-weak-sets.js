// El objeto WeakSet te deja almacenar y mantener objetos debilmente en un coleccion
const ws = new WeakSet();

// El metodo "add()" agrega un nuevo objeto al final de un Weakset objeto
let valor1 = ['valor 1', 1];
let valor2 = ['valor 2', 2];
let valor3 = ['valor 3', 3];

ws.add(valor1);
ws.add(valor2);

console.log(ws);

// El metodo "has()" devuelve un booleano que indica si un objeto existe en un WeakSet o no
console.log(ws.has(valor1));
console.log(ws.has(valor3));

// El metodo "delete()" elimina el elemento especificado de un Weakset objeto
ws.delete(valor2);
console.log(ws);

ws.add(valor2);
ws.add(valor3);
console.log(ws);