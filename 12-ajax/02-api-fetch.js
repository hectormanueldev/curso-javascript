(() => {
  const $fetch = document.getElementById('fetch');
  const $fragment = document.createDocumentFragment();

  fetch('https://jsonplaceholder.typicode.com/users')
  // El primer then transforma el json
  .then(res => res.ok ? res.json() : Promise.reject(res))
  // El segundo then maneja la logica de programacion
  .then((json) => {
    json.forEach((el) => {
      const $li = document.createElement('li');
      $li.innerHTML = `${el.name} -- ${el.email} -- ${el.phone}`;
      $fragment.appendChild($li);
    });

    $fetch.appendChild($fragment);
  })
  .catch((err) => {
    let message = err.statusText || 'Ocurrio un Error';
    $fetch.innerHTML = `Error: ${err.status}: ${message}`;
  })
  .finally(() => {
  });
})();