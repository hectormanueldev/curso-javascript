(() => {
  // Creamos la instancia
  const xhr = new XMLHttpRequest();
  const $xhr = document.getElementById('xhr');
  const $fragment = document.createDocumentFragment();

  // Creamos el evento
  xhr.addEventListener('readystatechange', (e) => {
    if (xhr.readyState !== 4) return;

    if (xhr.status >= 200 && xhr.status <= 300) {
      let usuariosJson = JSON.parse(xhr.responseText);

      usuariosJson.forEach((el) => {
        const $li = document.createElement('li');
        $li.innerHTML = `${el.name} -- ${el.email} -- ${el.phone}`;
        $fragment.appendChild($li);
      });

      $xhr.appendChild($fragment);
    } else {
      let message = xhr.statusText || 'Ocurrio un Error';
      $xhr.innerHTML = `Error ${xhr.status}: ${message}`;
    }
  });

  // Establecemos el metodo y el endpoint al que vamos a acceder
  xhr.open('GET', 'https://jsonplaceholder.typicode.com/users');
  // xhr.open('GET', './usuarios.json');

  // Enviamos la peticion
  xhr.send();
})();