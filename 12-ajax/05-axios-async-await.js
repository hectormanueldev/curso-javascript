(() => {
  const $axiosAsync = document.getElementById('axios-async');
  const $fragment = document.createDocumentFragment();

  async function getData() {
    try {
      let res = await axios.get('https://jsonplaceholder.typicode.com/users');
      let json = await res.data;

      json.forEach((el) => {
        const $li = document.createElement('li');
        $li.innerHTML = `${el.name} -- ${el.email} -- ${el.phone}`;
        $fragment.appendChild($li);
      });

      $axiosAsync.appendChild($fragment);
    } catch (err) {
      let message = err.response.statusText || 'Ocurrio un Error';
      $axiosAsync.innerHTML = `Error ${err.response.status}: ${message}`;
    } finally {
    }
  };

  getData();
})();