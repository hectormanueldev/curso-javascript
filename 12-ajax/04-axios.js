(() => {
  const $axios = document.getElementById('axios');
  const $fragment = document.createDocumentFragment();

  axios
  .get('https://jsonplaceholder.typicode.com/users')
  .then((res) => {
    let json = res.data;

    json.forEach((el) => {
      const $li = document.createElement('li');
      $li.innerHTML = `${el.name} -- ${el.email} -- ${el.phone}`;
      $fragment.appendChild($li);
    });

    $axios.appendChild($fragment);
  })
  .catch((err) => {
    let message = err.response.statusText || 'Ocurrio un Error';
    $axios.innerHTML = `Error ${err.response.status}: ${message}`;
  })
  .finally(() => {
  });
})();