/*
  5. Programa una funcion que invierta las palabras de una cadena de texto, por ejemplo, miFuncion('Hola mundo') devolvera 'odnum aloH'
*/

console.log('%cEjercicio 05', 'font-weight: bold; font-size: 18px');

const invertirCadena = (cadena = '') => (!cadena)
? console.warn('No ingresaste ningun texto')
: console.info(cadena.split('').reverse().join(''));

invertirCadena('Hola Mundo');

/*
  6. Programa una funcion para contar el numero de veces que se repite una palabra en un texto largo, por ejemplo, miFuncion('Hola mundo adios mundo', 'mundo') devolvera 2
*/

console.log('%cEjercicio 06', 'font-weight: bold; font-size: 18px');

const textoEnCadena = (cadena = '', palabra = undefined) => {
  if (!cadena) return console.warn('No ingresaste ningun texto');

  if (!palabra) return console.warn('No ingresaste ninguna palabra a evaluar');

  let i = 0;
  let contador = 0;

  while (i !== -1) {
    i = cadena.indexOf(palabra, i);

    if (i !== -1) {
      i++;
      contador++;
    }
  }

  return console.info(`La palabra ${palabra} se repite ${contador} veces en el texto`);
};

textoEnCadena('Hola mundo adios mundo', 'mundo');

/*
  7. Programa una funcion que valide si una palabra o frase dada, es un palindromo (que se lee igual en un sentido que en otro), por ejemplo, miFuncion('salas') devolvera true
*/

console.log('%cEjercicio 07', 'font-weight: bold; font-size: 18px');

const palindromo = (palabra = '') => {
  if (!palabra) return console.warn('No se ingreso ningun texto');

  palabra = palabra.toLowerCase();

  let alReves = palabra.split('').reverse().join('');

  return (palabra == alReves)
  ? console.info(`SI es palabra palindromo, palabra original "${palabra}", palabra al reves "${alReves}"`)
  : console.info(`NO es palabra palindromo, palabra original "${palabra}", palabra al reves "${alReves}"`);
};

palindromo('salas');
palindromo('Hola Mundo');

/*
  8. Programa una funcion que elimine cierto patron de caracteres de un texto dado, por ejemplo, miFuncion('xyz1, xyz2, xyz3, xyz4, y xyz5', 'xyz') devolvera 1, 2, 3, 4, y 5
*/

console.log('%cEjercicio 08', 'font-weight: bold; font-size: 18px');

const eliminarCaracteres = (cadena = '', patron = '') => (!cadena)
? console.warn('No se ingreso ningun texto')
: (!patron)
  ? console.warn('No se ingreso un patron de caracteres')
  : console.info(cadena.replace(new RegExp(patron, 'ig'), ''));

eliminarCaracteres('xyz1, xyz2, xyz3, xyz4, y xyz5', 'xyz');