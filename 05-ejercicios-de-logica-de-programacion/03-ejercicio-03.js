/*
  9. Programa una funcion que obtenga un numero aleatorio entre 501 y 600
*/

console.log('%cEjercicio 09', 'font-weight: bold; font-size: 18px');

const aleatorio = () => console.info(Math.round((Math.random() * 100)) + 500);

aleatorio();

/*
  10. Programa una funcion que reciba un numero y evalue si es capicua o no (que se lee igual en un sentido que en otro), por ejemplo, miFuncion(2002) devolvera true
*/

console.log('%cEjercicio 10', 'font-weight: bold; font-size: 18px');

const capicua = (numero = 0) => {
  if (!numero) return console.warn('No ingresaste ningun numero');

  if (typeof(numero) !== 'number') return console.error(`El valor ${numero} ingresado, NO es un numero`);

  numero = numero.toString();

  let alReves = numero.split('').reverse().join('');

  return (numero === alReves)
  ? console.info(`SI es numero capicua, numero original "${numero}", numero al reves "${alReves}"`)
  : console.info(`NO es numero capicua, numero original "${numero}", numero al reves "${alReves}"`)
};

capicua(2002);
capicua({});

/*
  11. Programa una funcion que calcule el factorial de un numero (el factorial de un entero positivo n, se define como el producto de todos los numeros enteros positivos desde 1 hasta n), por ejemplo, miFuncion(5) devolvera 120
*/

console.log('%cEjercicio 11', 'font-weight: bold; font-size: 18px');

const factorial = (numero = undefined) => {
  if (numero === undefined) return console.warn('No ingreso ningun numero');

  if (typeof(numero) !== 'number') return console.error(`El valor ${numero} ingresado, NO es un numero`);

  if (numero === 0) return console.error('El numero no puede ser 0');

  if (Math.sign(numero) === -1) return console.error('El numero no puede ser negativo');

  let factorial = 1;

  for (let i = numero; i > 1; i--) {
    factorial *= i;
  }

  return console.info(`El factorial de ${numero} es ${factorial}`);
};

factorial(5);
factorial(0);