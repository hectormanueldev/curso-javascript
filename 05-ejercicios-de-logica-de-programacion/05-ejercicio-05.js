/*
  15. Programa una funcion para convertir numeros de base binaria a decimal y viceversa, por ejemplo, miFuncion(100, 2) devolvera 4 base 10
*/

console.log('%cEjercicio 15', 'font-weight: bold; font-size: 18px');

const convertirBinarioDecimal = (numero = undefined, base = undefined) => {
  if (numero === undefined) return console.warn('No ingresaste ningun numero a convertir');

  if (typeof(numero) !== 'number') return console.error('El numero que se ingreso no es un numero');

  if (base === undefined) return console.warn('No ingresaste la base a convertir');

  if (typeof(base) !== 'number') return console.error('La base que se ingreso no es un numero');

  if (base === 2) {
    return console.info(`${numero} base ${base} = ${parseInt(numero, base)} base 10`);
  } else if (base === 10) {
    return console.info(`${numero} base ${base} = ${(numero.toString(2))} base 2`);
  } else {
    return console.error('El tipo de base a convertir no es valido');
  }
};

convertirBinarioDecimal(100, 2);

/*
  16. Programa una funcion que devuelva el monto final despues de aplicar un descuento a una cantidad dada, por ejemplo, miFuncion(1000, 20) devolvera 800
*/

console.log('%cEjercicio 16', 'font-weight: bold; font-size: 18px');

const aplicarDescuento = (monto = undefined, descuento = 0) => {
  if (monto === undefined) return console.warn('No ingresaste ningun monto');

  if (typeof(monto) !== 'number') return console.error('El monto que se ingreso no es un numero');

  if (monto === 0) return console.error('El monto no puede ser 0');

  if (Math.sign(monto) === -1) return console.error('El monto no puede ser negativo');

  if (typeof(descuento) !== 'number') return console.error('El descuento que se ingreso no es un numero');

  if (Math.sign(descuento) === -1) return console.error('El descuento no puede ser negativo');

  return console.info(`${monto} - ${descuento}% = ${monto - ((monto * descuento) / 100)}`);
};

aplicarDescuento(1000, 20);

/*
  17. Programa una funcion que dada una fecha valida determine cuantos años han pasado hasta el dia de hoy, por ejemplo, miFuncion(new Date(1984, 4, 23)) devolvera 35 años en 2020
*/

console.log('%cEjercicio 17', 'font-weight: bold; font-size: 18px');

const calcularAnios = (fecha = undefined) => {
  if (fecha === undefined) return console.warn('No ingresaste ninguna fecha');

  if (!(fecha instanceof Date)) return console.error('El valor que ingresaste no es una fecha valida');

  let hoyMenosFecha = new Date().getTime() - fecha.getTime();
  let aniosEnMS = 1000 * 60 * 60 * 24 * 365;
  let aniosHumanos = Math.floor(hoyMenosFecha / aniosEnMS);

  return (Math.sign(aniosHumanos) === -1)
  ? console.info(`Faltan ${Math.abs(aniosHumanos)} años para el ${fecha.getFullYear()}`)
  : (Math.sign(aniosHumanos) === 1)
    ? console.info(`Han pasado ${Math.abs(aniosHumanos)} años desde el ${fecha.getFullYear()}`)
    : console.info(`Estamos en el año actual ${fecha.getFullYear()}`);
};

calcularAnios(new Date(2000, 10, 13));
calcularAnios(new Date(2050, 10, 13));