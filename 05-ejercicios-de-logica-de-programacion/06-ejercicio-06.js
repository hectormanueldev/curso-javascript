/*
  18. Programa una funcion que dada una cadena de texto cuente el numero de vocales y consonantes, por ejemplo, miFuncion('Hola mundo') devolvera vocales:4, consonantes: 5
*/

console.log('%cEjercicio 18', 'font-weight: bold; font-size: 18px');

const contarLetras = (cadena = '') => {
  if (cadena === undefined) return console.warn('No ingresaste ninguna cadena');

  if (typeof(cadena) !== 'string') return console.error('La cadena ingresada no es una cadena de texto');

  let vocales = 0;
  let consonantes = 0;

  cadena = cadena.toLowerCase();

  for (let letra of cadena) {
    if (/[aeiouáéíóúü]/.test(letra)) vocales++;

    if (/[bcdfghjklmnñpqrstvwxyz]/.test(letra)) consonantes++;
  }

  return console.info({
    cadena,
    vocales,
    consonantes
  });
};

contarLetras('Hola Mundo');

/*
  19. Programa una funcion que valide que un texto sea un nombre valido, por ejemplo, miFuncion('Jonathan Mircha') devolvera verdadero
*/

console.log('%cEjercicio 19', 'font-weight: bold; font-size: 18px');

const validarNombre = (nombre = '') => {
  if (nombre === undefined) return console.warn('No ingresaste ningun nombre');

  if (typeof(nombre) !== 'string') return console.error('La cadena ingresada no es un nombre');

  let expReg = /^[A-Za-zÑñÁáÉéÍíÓóÚúÜü\s]+$/g.test(nombre);

  return (expReg)
  ? console.info(`"${nombre}", es un nombre valido`)
  : console.info(`"${nombre}", no es un nombre valido`);
};

validarNombre('Hector Manuel');

/*
  20. Programa una funcion que valide que un texto sea un email valido, por ejemplo, miFuncion('hecman@gmail.com') devolvera verdadero
*/

console.log('%cEjercicio 20', 'font-weight: bold; font-size: 18px');

const validarCorreo = (correo = '') => {
  if (correo === undefined) return console.warn('No ingresaste ningun correo');

  if (typeof(correo) !== 'string') return console.error('La cadena ingresada no es un correo');

  let expReg = /[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})/i.test(correo);

  return (expReg)
  ? console.info(`"${correo}", es un correo valido`)
  : console.info(`"${correo}", no es un correo valido`);
};

validarCorreo('hectormanuelsantosbautista27@gmail.com');
validarCorreo('hector.manuel.santos.bautista@gmail.com');