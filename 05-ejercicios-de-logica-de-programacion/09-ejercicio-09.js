/*
  27. Programa una clase llamada Pelicula, la clase recibira un objeto al momento de instanciarse con los siguientes datos: id de la pelicula en IMDB, titulo, director, año de estreno, pais o paises de origen, generos y calificacion en IMDB.

  -Todos los datos del objeto son obligarios.
  -Valida que el id IMDB tenga 9 caracteres, los primeros dos sean letras y los 7 restantes numeros.
  -Valida que el titulo no rebase los 100 caracteres.
  -Valida que el director no rebase los 50 caracteres.
  -Valida que el año de estreno sea un numero entero de 4 digitos.
  -Valida que el pais o paises sean introducidos en forma de arreglo.
  -Valida que los generos sean introducidos en forma de arreglo.
  -Valida que los generos introducidos esten dentro de los generos aceptados.
  -Crea un metodo estatico que devuelve los generos aceptados.
  -Valida que la calificacion sea un numero entre 0 y 10 pudiendo ser decimal de una posicion.
  -Crea un metodo que devuelva toda la ficha tecnica de la pelicula.
  -A partir de un arreglo con la informacion de 3 peliculas genera 3 instancias de la clase de forma automatizada e imprime la ficha tecnica de cada pelicula.

  Generos aceptados:
  Action, Adult, Adventure, Animation, Biography, Comedy, Crime, Documentary, Drama, Family, Fantasy, Film Noir, Game-Show, History, Horror, Musical, Music, Mystery, News, Reality-TV, Romance, Sci-Fi, Short, Sport, Talk-Show, Thriller, War, Western
*/

console.log('%cEjercicio 27', 'font-weight: bold; font-size: 18px');

class Pelicula {
  constructor({ id, titulo, director, estreno, pais, generos, calificacion }) {
    this.id = id;
    this.titulo = titulo;
    this.director = director;
    this.estreno = estreno;
    this.pais = pais;
    this.generos = generos;
    this.calificacion = calificacion;

    this.validarIMDB(id);
    this.validarTitulo(titulo);
    this.validarDirector(director);
    this.validarEstreno(estreno);
    this.validarPais(pais);
    this.validarGeneros(generos);
    this.validarCalificacion(calificacion);
  }

  static get listaGeneros() {
    return ['Action', 'Adult', 'Adventure', 'Animation', 'Biography', 'Comedy', 'Crime', 'Documentary', 'Drama', 'Family', 'Fantasy', 'Film Noir', 'Game-Show', 'History', 'Horror', 'Musical', 'Music', 'Mystery', 'News', 'Reality-TV', 'Romance', 'Sci-Fi', 'Short', 'Sport', 'Talk-Show', 'Thriller', 'War', 'Western'];
  };

  static generosAceptados() {
    return console.info(`Los generos aceptados son: ${Pelicula.listaGeneros.join(', ')}`);
  };

  validarCadena(propiedad, valor) {
    if (!valor) return console.warn(`${propiedad} "${valor}" esta vacio`);

    if (typeof valor !== 'string') return console.error(`${propiedad} "${valor}" ingresado, NO es una cadena de texto`);

    return true;
  };

  validarLongitudCadena(propiedad, valor, longitud) {
    if (valor.length > longitud) return console.error(`${propiedad} "${valor}" excede el numero de caracteres permitidos (${longitud})`);

    return true;
  };

  validarNumero(propiedad, valor) {
    if (!valor) return console.warn(`${propiedad} "${valor}" esta vacio`);

    if (typeof valor !== 'number') return console.error(`${propiedad} "${valor}" ingresado, NO es un numero`);

    return true;
  };

  validarArreglo(propiedad, valor) {
    if (!valor) return console.warn(`${propiedad} "${valor}" esta vacío`);

    if (!(valor instanceof Array)) return console.error(`${propiedad} "${valor}" ingresado, NO es un arreglo`);

    if (valor.length === 0) return console.error(`${propiedad} "${valor}" no tiene datos`);

    for (let cadena of valor) {
      if (typeof cadena !== 'string') return console.error(`El valor ${cadena} ingresado, NO es un cadena de texto`);
    }

    return true;
  };

  validarIMDB(id) {
    if (this.validarCadena('IMDB id', id)) {
      if (!(/^([a-z]){2}([0-9]){7}$/.test(id))) return console.error(`IMDB id "${id}" no es valido, debe tener 9 caracteres, los dos primeros letras minusculas, los 7 restantes numeros`);
    }
  };

  validarTitulo(titulo) {
    if (this.validarCadena('Titulo', titulo)) {
      this.validarLongitudCadena('Titlo', titulo, 100);
    }
  };

  validarDirector(director) {
    if (this.validarCadena('Director', director)) {
      this.validarLongitudCadena('Director', director, 50);
    }
  };

  validarEstreno(estreno) {
    if (this.validarNumero('Año de estreno', estreno)) {
      if (!/^([0-9]){4}$/.test(estreno)) return console.error(`Año de estreno "${estreno}" no es válido, debe ser un numero de 4 digitos`);
    }
  };

  validarPais(pais) {
    this.validarArreglo('Pais', pais);
  };

  validarGeneros(generos) {
    if (this.validarArreglo('Géneros', generos)) {
      for (let genero of generos) {
        if (!Pelicula.listaGeneros.includes(genero)) {
          console.error(`Géneros incorrectos "${generos.join(', ')}"`);
          Pelicula.generosAceptados();
        }
      }
    }
  };

  validarCalificacion(calificacion) {
    if (this.validarNumero('Calificación', calificacion)) return calificacion < 0 || calificacion > 10
      ? console.error('La calificación tiene que estar entre 0 y 10')
      : (this.calificacion = calificacion.toFixed(1));
  };

  fichaTecnica() {
    console.info(`
      Ficha Técnica:
      Título: ${this.titulo}
      Director: ${this.director}
      Año: ${this.estreno}
      Pais: ${this.pais.join(', ')}
      Géneros: ${this.generos.join(', ')}
      Calificación: ${this.calificacion}
      IMDB id: ${this.id}
    `);
  };
}

const misPelis = [
  {
    id: 'tt1234567',
    titulo: 'Titulo',
    director: 'Director',
    estreno: 2019,
    pais: ['Mexico'],
    generos: ['Comedy', 'Sport'],
    calificacion: 5
  },
  {
    id: 'tt1234567',
    titulo: 'Titulo',
    director: 'Director',
    estreno: 2020,
    pais: ['Mexico'],
    generos: ['Comedy', 'Sport'],
    calificacion: 6
  },
  {
    id: 'tt1234567',
    titulo: 'Titulo',
    director: 'Director',
    estreno: 2021,
    pais: ['Mexico'],
    generos: ['Comedy', 'Sport'],
    calificacion: 8
  }
];

misPelis.forEach(el => new Pelicula(el).fichaTecnica());