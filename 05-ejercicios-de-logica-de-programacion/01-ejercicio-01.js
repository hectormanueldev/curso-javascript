/*
  1. Programa una funcion que cuente el numero de caracteres de una cadena de texto, por ejemplo, miFuncion('Hola Mundo') devolvera 10
*/

console.log('%cEjercicio 01', 'font-weight: bold; font-size: 18px');

const contarCaracteres = (cadena = '') => (!cadena)
? console.warn('No ingresaste ningun texto')
: console.info(`La cadena "${cadena}" tiene ${cadena.length} caracteres`);

contarCaracteres('Hola Mundo');

/*
  2. Programa una funcion que te devuelva el texto recortado segun el numero de caracteres indicados, por ejemplo, miFuncion('Hola Mundo', 4) devolvera 'Hola'
*/

console.log('%cEjercicio 02', 'font-weight: bold; font-size: 18px');

const recortarTexto = (cadena = '', longitud = undefined) => (!cadena)
? console.warn('No ingresaste ningun texto')
: (longitud === undefined)
  ? console.warn('No ingresaste la longitud para recortar el texto')
  : console.info(cadena.slice(0, longitud));

recortarTexto('Hola Mundo', 4);

/*
  3. Programa una funcion que dado un String te devuelva un array de textos separados por cierto caracter, por ejemplo, miFuncion('Hola que tal', ' ') devolvera ['hola', 'que', 'tal']
*/

console.log('%cEjercicio 03', 'font-weight: bold; font-size: 18px');

const cadenaAArreglo = (cadena = '', separador = undefined) => (!cadena)
? console.warn('No ingresaste ningun texto')
: (separador === undefined)
  ? console.warn('No ingresaste el caracter separador')
  : console.info(cadena.split(separador));

cadenaAArreglo('Hola que tal', ' / ');

/*
  4. Programa una funcion que repita un texto x veces, por ejemplo, miFuncion('Hola mundo', 3) devolvera 'Hola mundo Hola mundo Hola mundo'
*/

console.log('%cEjercicio 04', 'font-weight: bold; font-size: 18px');

const repetirTexto = (cadena = '', veces = undefined) => {
  if (!cadena) return console.warn('No ingresaste ningun texto');

  if (veces === undefined) return console.warn('No ingresaste las veces a repetir la cadena');

  if (veces === 0) return console.error('El numero de veces no puede ser 0');

  if (Math.sign(veces) === -1) return console.error('El numero de veces no puede ser negativo');

  for (let i = 1; i <= veces; i++) {
    console.info(`${cadena}, ${i}`);
  }
};

repetirTexto('Hola Mundo', 3);