/*
  21. Programa una funcion que dado un array numerico devuelve otro array con los numeros elevado al cuadrado, por ejemplo, miFuncion([1, 4, 5]) devolvera [1, 16, 25]
*/

console.log('%cEjercicio 21', 'font-weight: bold; font-size: 18px');

const devolverCuadrados = (arr = undefined) => {
  if (arr === undefined) return console.warn('No ingresaste un arreglo de numeros');

  if (!(arr instanceof Array)) return console.error('Los datos ingresados no corresponden a un array');

  if (arr.length === 0) return console.error('El arreglo esta vacio');

  for (let num of arr) {
    if (typeof(num) !== 'number') return console.error('El valor del array no es un numero');
  }

  const newArray = arr.map(el => el * el);

  return console.info(`Arreglo original: ${arr}, arreglo elevado al cuadrado: ${newArray}`);
};

devolverCuadrados([1, 4, 5]);

/*
  22. Programa una funcion que dado un array devuelva el numero mas alto y el numero mas bajo de dicho array, por ejemplo, miFuncion([1, 4, 5, 99, -60]) devolvera [99, -60]
*/

console.log('%cEjercicio 22', 'font-weight: bold; font-size: 18px');

const arrayMinMax = (arr = undefined) => {
  if (arr === undefined) return console.warn('No ingresaste un arreglo de numeros');

  if (!(arr instanceof Array)) return console.error('Los datos ingresados no corresponden a un array');

  if (arr.length === 0) return console.error('El arreglo esta vacio');

  for (let num of arr) {
    if (typeof(num) !== 'number') return console.error('El valor del array no es un numero');
  }

  return console.info(`Arreglo original: ${arr}, numero mas alto: ${Math.max(...arr)}, numero mas bajo: ${Math.min(...arr)}`);
};

arrayMinMax([1, 4, 5, 99, -60]);

/*
  23. Programa una funcion que dado un array de numeros devuelva un objeto con dos arrays, en el primer array almacena los numeros pares y en el segundo array los impares, por ejemplo, miFuncion([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]) devolvera {pares: [2, 4, 6, 8, 0], impares: [1, 3, 5, 7, 9]}
*/

console.log('%cEjercicio 23', 'font-weight: bold; font-size: 18px');

const separaParesImpares = (arr = undefined) => {
  if (arr === undefined) return console.warn('No ingresaste un arreglo de numeros');

  if (!(arr instanceof Array)) return console.error('Los datos ingresados no corresponden a un array');

  if (arr.length === 0) return console.error('El arreglo esta vacio');

  for (let num of arr) {
    if (typeof(num) !== 'number') return console.error('El valor del array no es un numero');
  }

  return console.info({
    pares: arr.filter(num => num % 2 === 0),
    impares: arr.filter(num => num % 2 === 1)
  });
};

separaParesImpares([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]);