/*
  24. Programa una funcion que dado un arreglo de numero devuelva un objeto con dos arreglos, el primero tendra los numeros ordenados de forma ascendente, y el segundo de forma descendiente, por ejemplo, miFuncion([7, 5, 7, 8, 6]) devolvera {asc: [5, 6, 7, 7, 8], desc: [8, 7, 7, 6, 5]}
*/

console.log('%cEjercicio 24', 'font-weight: bold; font-size: 18px');

const ordernarArreglo = (arr = undefined) => {
  if (arr === undefined) return console.warn('No ingresaste un arreglo de numeros');

  if (!(arr instanceof Array)) return console.error('Los datos ingresados no corresponden a un array');

  if (arr.length === 0) return console.error('El arreglo esta vacio');

  for (let num of arr) {
    if (typeof(num) !== 'number') return console.error('El valor del array no es un numero');
  }

  return console.info({
    arr,
    asc: arr.map(el => el).sort(),
    desc: arr.map(el => el).sort().reverse()
  });
};

ordernarArreglo([7, 5, 7, 8, 6]);

/*
  25. Programa una funcion que dado un arreglo de elementos, elimine los duplicados, por ejemplo, miFuncion(['x', 10, 'x', 2, '10', 10, true, true]) devolvera ['x', 10, 2, '10', true]
*/

console.log('%cEjercicio 25', 'font-weight: bold; font-size: 18px');

const eliminarDuplicados = (arr = undefined) => {
  if (arr === undefined) return console.warn('No ingresaste un arreglo de numeros');

  if (!(arr instanceof Array)) return console.error('Los datos ingresados no corresponden a un array');

  if (arr.length === 0) return console.error('El arreglo esta vacio');

  if (arr.length === 1) return console.error('El arreglo debe tener al menos dos elementos');

  // return console.info({
  //   original: arr,
  //   sinDuplicados: arr.filter((value, index, self) => self.indexOf(value) === index)
  // });

  return console.info({
    original: arr,
    sinDuplicados: [...new Set(arr)]
  });
};

eliminarDuplicados(['x', 10, 'x', 2, '10', 10, true, true]);

/*
  26. Programa una funcion que dado un arreglo de numeros obtenga el promedio, por ejemplo, miFuncion([9, 8, 7, 6, 5, 4, 3, 2, 1, 0]) devolvera 4.5
*/

console.log('%cEjercicio 26', 'font-weight: bold; font-size: 18px');

const calcularPromedio = (arr = undefined) => {
  if (arr === undefined) return console.warn('No ingresaste un arreglo de numeros');

  if (!(arr instanceof Array)) return console.error('Los datos ingresados no corresponden a un array');

  if (arr.length === 0) return console.error('El arreglo esta vacio');

  return console.info(
    arr.reduce((total, num, index, arr) => {
      total += num;

      if (index === arr.length - 1) {
        return `El promedio de: ${arr.join(' + ')} es ${total / arr.length}`;
      } else {
        return total;
      }
    })
  );
};

calcularPromedio([9, 8, 7, 6, 5, 4, 3, 2, 1, 0]);