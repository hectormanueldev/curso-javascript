/*
  12. Programa una funcion que determine si un numero es primo (aquel que solo es divisible por si mismo y 1) o no, por ejemplo, miFuncion(7) devolvera true
*/

console.log('%cEjercicio 12', 'font-weight: bold; font-size: 18px');

const numeroPrimo = (numero = undefined) => {
  if (numero === undefined) return console.warn('No ingresaste ningun numero');

  if (typeof(numero) !== 'number') return console.error('El numero que se ingreso no es un numero');

  if (numero === 0) return console.error('El numero ingresado no puede ser 0');

  if (Math.sign(numero) === -1) return console.error('El numero ingresado no puede ser negativo');

  let divisible = false;

  for (let i = 2; i < numero; i++) {
    if ((numero % i) === 0) {
      divisible = true;
      break;
    }
  }

  return (divisible)
  ? console.info(`El numero ${numero} no es primo`)
  : console.info(`El numero ${numero} es primo`);
};

numeroPrimo(7);

/*
  13. Programa una funcion que determine si un numero es par o impar, por ejemplo, miFuncion(29) devolvera impar
*/

console.log('%cEjercicio 13', 'font-weight: bold; font-size: 18px');

const numeroParImpar = (numero = undefined) => {
  if (numero === undefined) return console.warn('No ingresaste ningun numero');

  if (typeof(numero) !== 'number') return console.error('El numero que se ingreso no es un numero');

  return (numero % 2 === 0)
  ? console.info(`El numero ${numero} es par`)
  : console.info(`El numero ${numero} es impar`);
};

numeroParImpar(29);
numeroParImpar(20);

/*
  14. Programa una funcion para convertir grados Celcius a Fahrenheit y viceversa, por ejemplo, miFuncion(0, 'c') devolvera 32 grados Fahrenheit
*/

console.log('%cEjercicio 14', 'font-weight: bold; font-size: 18px');

const conversionTemperatura = (grados = undefined, unidad = undefined) => {
  if (grados === undefined) return console.warn('No ingresaste grados a convertir');

  if (typeof(grados) !== 'number') return console.error('El valor de grados que se ingreso no es un numero');

  if (unidad === undefined) return console.warn('No ingresaste la unidad a convertir');

  if (typeof(unidad) !== 'string') return console.error('El valor de unidad que se ingreso no es una unidad');

  if (unidad.length !== 1 || !/(C|F)/.test(unidad)) return console.warn('Valor de unidad no reconocido');

  if (unidad === 'C') {
    return console.info(`${grados}°C = ${Math.round((grados * 9 / 5) + 32)}°F`);
  } else if (unidad === 'F') {
    return console.info(`${grados}°F = ${Math.round((grados - 32) * 5 / 9)}°C`);
  }
};

conversionTemperatura(0, 'C');
conversionTemperatura(32, 'F');