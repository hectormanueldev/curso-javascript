# Curso de JavaScript

## Temario

1. :heavy_plus_sign: Tipos de datos
1. :heavy_plus_sign: Estructuras de control
1. :heavy_plus_sign: Programación orientada a objetos
1. :heavy_plus_sign: Objetos y funciones del lenguaje
1. :heavy_plus_sign: Ejercicios de lógica de programación
1. :heavy_plus_sign: Programación asíncrona
1. :heavy_plus_sign: Nuevos tipos y características
1. :heavy_plus_sign: This en JavaScript
1. :heavy_plus_sign: JSON
1. :heavy_plus_sign: DOM
1. :heavy_plus_sign: Ejercicios prácticos del DOM
1. :heavy_plus_sign: AJAX
1. :heavy_plus_sign: API REST
1. :heavy_plus_sign: Ejercicios prácticos de AJAX y API REST
1. :heavy_plus_sign: Single page application
1. :heavy_plus_sign: Reactividad
