import api from '../helpers/wp_api.js';
import { ajax } from '../helpers/ajax.js';
import { PostCard } from './PostCard.js';
import { Post } from './Post.js';
import { SearchCard } from './SearchCard.js';
import { ContactForm } from './ContactForm.js';

export async function Router() {
  const d = document;

  let { hash } = location;

  d.getElementById('main').innerHTML = null;

  if (!hash || hash === '#/') {
    await ajax({
      url: api.POSTS,
      callbackSuccess: (posts) => {
        let html = '';
        posts.forEach(post => html += PostCard(post));
        d.getElementById('main').innerHTML = html;
      }
    });
  } else if (hash.includes('#/search')) {
    let query = localStorage.getItem('wpSearch');

    if (!query) {
      d.querySelector('.loader').style.display = 'none';
      return false;
    }

    await ajax({
      url: `${api.SEARCH}${query}`,
      callbackSuccess: (search) => {
        let html = '';

        if (search.length === 0) {
          html = `
            <p class="error">
              No existen resultados para el termino <mark>${query}</mark>
            </p>
          `;
        } else {
          search.forEach((post) => (html += SearchCard(post)));
        }

        d.getElementById('main').innerHTML = html;
      }
    });
  } else if (hash === '#/contacto') {
    setTimeout(() => {
      d.getElementById('main').appendChild(ContactForm());
    }, 100);
  } else {
    await ajax({
      url: `${api.POST}/${localStorage.getItem('wpPostId')}`,
      callbackSuccess: (post) => {
        d.getElementById('main').innerHTML = Post(post);
      }
    });
  }

  d.querySelector('.loader').style.display = 'none';
};